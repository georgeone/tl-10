

#include "nrf_adc.h"
#include "nrf_drv_config.h"
#include "sdk_errors.h"
#include <stdbool.h>




typedef enum
{
    NRF_DRV_ADC_EVT_DONE,    ///< Event generated when the buffer is filled with samples.
    NRF_DRV_ADC_EVT_SAMPLE,  ///< Event generated when the requested channel is sampled.
} nrf_drv_adc_evt_type_t;

typedef int16_t nrf_adc_value_t;

/**
 * @brief Analog-to-digital converter driver DONE event.
 */
typedef struct
{
    nrf_adc_value_t *        p_buffer; ///< Pointer to buffer with converted samples.
    uint16_t                 size;     ///< Number of samples in the buffer.
} nrf_drv_adc_done_evt_t;

/**
 * @brief Analog-to-digital converter driver SAMPLE event.
 */
typedef struct
{
    nrf_adc_value_t   sample; ///< Converted sample.
} nrf_drv_adc_sample_evt_t;

/**
 * @brief Analog-to-digital converter driver event.
 */
typedef struct
{
    nrf_drv_adc_evt_type_t type;  ///< Event type.
    union
    {
        nrf_drv_adc_done_evt_t   done;   ///< Data for DONE event.
        nrf_drv_adc_sample_evt_t sample; ///< Data for SAMPLE event.
    } data;
} nrf_drv_adc_evt_t;

/**@brief Macro for initializing the ADC channel with the default configuration. */
#define NRF_DRV_ADC_DEFAULT_CHANNEL(analog_input)          \
 {{{                                                       \
    .resolution = NRF_ADC_CONFIG_RES_10BIT,                \
    .input      = NRF_ADC_CONFIG_SCALING_INPUT_ONE_THIRD, \
    .reference  = NRF_ADC_CONFIG_REF_SUPPLY_ONE_HALF,                  \
    .ain        = (analog_input)                           \
 }}, NULL}

/**
 * @brief ADC channel configuration.
 *
 * @note The bit fields reflect bit fields in the ADC CONFIG register.
 */
typedef struct 
{
    uint32_t resolution        :2; ///< 8-10 bit resolution.
    uint32_t input             :3; ///< Input selection and scaling.
    uint32_t reference         :2; ///< Reference source.
    uint32_t reserved          :1; ///< Unused bit fields.
    uint32_t ain               :8; ///< Analog input.
    uint32_t external_reference:2; ///< Eternal reference source.
}nrf_drv_adc_channel_config_t;

// Forward declaration of the nrf_drv_adc_channel_t type.
typedef struct nrf_drv_adc_channel_s nrf_drv_adc_channel_t;

/**
 * @brief ADC channel.
 *
 * This structure is defined by the user and used by the driver. Therefore, it should
 * not be defined on the stack as a local variable.
 */
struct nrf_drv_adc_channel_s
{
    union
    {
        nrf_drv_adc_channel_config_t config; ///< Channel configuration.
        uint32_t data;                       ///< Raw value.
    } config;
    nrf_drv_adc_channel_t      * p_next;     ///< Pointer to the next enabled channel (for internal use).
};

/**
 * @brief ADC configuration.
 */
typedef struct 
{
    uint8_t interrupt_priority;              ///< Priority of ADC interrupt.
} nrf_drv_adc_config_t;

/** @brief ADC default configuration. */
#define NRF_DRV_ADC_DEFAULT_CONFIG                \
{                                                 \
    .interrupt_priority = ADC_CONFIG_IRQ_PRIORITY \
}


 
typedef void (*nrf_drv_adc_event_handler_t)(nrf_drv_adc_evt_t const * p_event);


ret_code_t nrf_drv_adc_init(nrf_drv_adc_config_t const * p_config,
                            nrf_drv_adc_event_handler_t  event_handler);


 
void nrf_drv_adc_uninit(void);


 
void nrf_drv_adc_channel_enable(nrf_drv_adc_channel_t * const p_channel);

/**
 * @brief Function for disabling an ADC channel.
 */
void nrf_drv_adc_channel_disable(nrf_drv_adc_channel_t * const p_channel);

void nrf_drv_adc_sample(void);


ret_code_t nrf_drv_adc_sample_convert(nrf_drv_adc_channel_t const * const p_channel, 
                                      nrf_adc_value_t * p_value);


ret_code_t nrf_drv_adc_buffer_convert(nrf_adc_value_t * buffer, uint16_t size);


bool nrf_drv_adc_is_busy(void);


 
__STATIC_INLINE uint32_t nrf_drv_adc_start_task_get(void);


__STATIC_INLINE nrf_adc_config_input_t nrf_drv_adc_gpio_to_ain(uint32_t pin);

#ifndef SUPPRESS_INLINE_IMPLEMENTATION

__STATIC_INLINE uint32_t nrf_drv_adc_start_task_get(void)
{
    return nrf_adc_task_address_get(NRF_ADC_TASK_START);
}

__STATIC_INLINE nrf_adc_config_input_t nrf_drv_adc_gpio_to_ain(uint32_t pin)
{
    // AIN2 - AIN7
    if (pin >= 1 && pin <= 6)
    {
        return (nrf_adc_config_input_t)(1 << (pin+1));
    }
    // AIN0 - AIN1
    else if (pin >= 26 && pin <= 27)
    {
        return (nrf_adc_config_input_t)(1 <<(pin - 26));
    }
    else
    {
        return NRF_ADC_CONFIG_INPUT_DISABLED;
    }
}
#endif
/** @} */
