


#define BUTTON_1   	   12
#define COIL			11
#define LED_PWR        13
#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define LEDS_MASK      (BSP_LED_0_MASK)
#define RX_PIN_NUMBER  8
#define TX_PIN_NUMBER  9
#define CTS_PIN_NUMBER 8
#define RTS_PIN_NUMBER 8
#define HWFC           true
// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_RC,            \
                                 .rc_ctiv       = 40,                                \
                                 .rc_temp_ctiv  = 1,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_250_PPM}
                                 // rc_ctv: 40 means Calibration timer interval for each 10 seconds. 
 //Low frequency clock source to be used by the SoftDevice
//#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_XTAL,            \
//															 .rc_ctiv       = 0,                                \
//															 .rc_temp_ctiv  = 0,                                \
//															 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}
//#define BUTTON_1   	   17
//#define BSP_LED_1      LED_PWR2
//#define BSP_LED_0_MASK (1<<BSP_LED_0)
//#define LEDS_MASK      (BSP_LED_0_MASK|BSP_LED_1_MASK)
//#define COIL			24
//#define LED_PWR					21
//#define LED_PWR2					22
//#define RX_PIN_NUMBER  11
//#define BSP_LED_1_MASK (1<<BSP_LED_1)
//#define TX_PIN_NUMBER  9
//#define CTS_PIN_NUMBER 10
//#define RTS_PIN_NUMBER 8
//#define HWFC           true


// LEDs definitions

#define LEDS_NUMBER    1
#define LED_START      13



#define LED_STOP       13

#define LEDS_LIST {LED_PWR }
#define BSP_LED_0      LED_PWR



#define LEDS_INV_MASK  LEDS_MASK


#define BUTTONS_NUMBER 1
#define BUTTON_START   12
#define BUTTON_STOP    12
#define BUTTON_PULL    NRF_GPIO_PIN_NOPULL
#define BUTTONS_LIST { BUTTON_1}
#define BSP_BUTTON_0   BUTTON_1


#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)


#define BUTTONS_MASK   0x001E0000
//#define BUTTONS_MASK   (BSP_BUTTON_0_MASK | BSP_BUTTON_1_MASK |BSP_BUTTON_2_MASK | BSP_BUTTON_3_MASK | BSP_BUTTON_4_MASK)
//Motors
#define MOT_1           13
#define MOT_2           12
#define MOT_3           11



#if 0
#define SPIS_MISO_PIN  28    // SPI MISO signal. 
#define SPIS_CSN_PIN   12    // SPI CSN signal. 
#define SPIS_MOSI_PIN  25    // SPI MOSI signal. 
#define SPIS_SCK_PIN   29    // SPI SCK signal. 

#define SPIM0_SCK_PIN       4     /**< SPI clock GPIO pin number. */
#define SPIM0_MOSI_PIN      1     /**< SPI Master Out Slave In GPIO pin number. */
#define SPIM0_MISO_PIN      3     /**< SPI Master In Slave Out GPIO pin number. */
#define SPIM0_SS_PIN        2     /**< SPI Slave Select GPIO pin number. */

#define SPIM1_SCK_PIN       15     /**< SPI clock GPIO pin number. */
#define SPIM1_MOSI_PIN      12     /**< SPI Master Out Slave In GPIO pin number. */
#define SPIM1_MISO_PIN      14     /**< SPI Master In Slave Out GPIO pin number. */
#define SPIM1_SS_PIN        13     /**< SPI Slave Select GPIO pin number. */

// serialization APPLICATION board
#define SER_CONN_CHIP_RESET_PIN     12    // Pin used to reset connectivity chip

#define SER_APP_RX_PIN              25    // UART RX pin number.
#define SER_APP_TX_PIN              28    // UART TX pin number.
#define SER_APP_CTS_PIN             0     // UART Clear To Send pin number.
#define SER_APP_RTS_PIN             29    // UART Request To Send pin number.

#define SER_APP_SPIM0_SCK_PIN       7     // SPI clock GPIO pin number.
#define SER_APP_SPIM0_MOSI_PIN      0     // SPI Master Out Slave In GPIO pin number
#define SER_APP_SPIM0_MISO_PIN      30    // SPI Master In Slave Out GPIO pin number
#define SER_APP_SPIM0_SS_PIN        25    // SPI Slave Select GPIO pin number
#define SER_APP_SPIM0_RDY_PIN       29    // SPI READY GPIO pin number
#define SER_APP_SPIM0_REQ_PIN       28    // SPI REQUEST GPIO pin number

// serialization CONNECTIVITY board
#define SER_CON_RX_PIN              28    // UART RX pin number.
#define SER_CON_TX_PIN              25    // UART TX pin number.
#define SER_CON_CTS_PIN             29    // UART Clear To Send pin number. Not used if HWFC is set to false.
#define SER_CON_RTS_PIN             0    // UART Request To Send pin number. Not used if HWFC is set to false.


#define SER_CON_SPIS_SCK_PIN        7     // SPI SCK signal.
#define SER_CON_SPIS_MOSI_PIN       0     // SPI MOSI signal.
#define SER_CON_SPIS_MISO_PIN       30    // SPI MISO signal.
#define SER_CON_SPIS_CSN_PIN        25    // SPI CSN signal.
#define SER_CON_SPIS_RDY_PIN        29    // SPI READY GPIO pin number.
#define SER_CON_SPIS_REQ_PIN        28    // SPI REQUEST GPIO pin number.
#endif


