
#include "adc.h"
#include "nrf_soc.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"

#define BATT_AIN                ADC_CONFIG_PSEL_AnalogInput7

bat_table_t bat_table[] = 
{  	
    { 0x56, 100 },   
		{ 0x54,  80 },  
    { 0x53,  70 },  
		{ 0x52,  60 },  
    { 0x51,  50 },  	
    { 0x50,  40 },  
		{ 0x4F,  30 },  
    { 0x4E,  10 }, 
		{ 0x4D,   1 },  
};


static uint8_t  original_bat_adc; 
static uint8_t  bat_percentage;



// *****************************************************************************
// Function 	: battery_adc_to_percent
// Input		: 
// Output		: 
// Note			: 
// *****************************************************************************
void batt_adc_to_percent(uint16_t original_adc)
{

		bat_percentage = original_adc;


}



void batt_adc_check(){	
    // interrupt ADC
	NRF_ADC->INTENSET = (ADC_INTENSET_END_Disabled << ADC_INTENSET_END_Pos);						//!< Interrupt enabled. 


	// config ADC
		NRF_ADC->CONFIG = (ADC_CONFIG_EXTREFSEL_None << ADC_CONFIG_EXTREFSEL_Pos) // Bits 17..16 : ADC external reference pin selection. 
		| (BATT_AIN << ADC_CONFIG_PSEL_Pos)				//	!< Use analog input 3 as analog input. 
		| (ADC_CONFIG_REFSEL_SupplyOneHalfPrescaling << ADC_CONFIG_REFSEL_Pos)						//	!< Use internal 1.2V bandgap voltage as reference for conversion. 
		| (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) ///*!< Analog input specified by PSEL with 1/3 prescaling used as input for the conversion. 
		| (ADC_CONFIG_RES_10bit << ADC_CONFIG_RES_Pos);					//				!< 8bit ADC resolution. 
  
/*
    NRF_ADC->CONFIG = (ADC_CONFIG_EXTREFSEL_AnalogReference0 << ADC_CONFIG_EXTREFSEL_Pos) // Bits 17..16 : ADC external reference pin selection. 
		| (BATT_AIN << ADC_CONFIG_PSEL_Pos)				//	!< Use analog input 2 as analog input. 
		| (ADC_CONFIG_REFSEL_External << ADC_CONFIG_REFSEL_Pos)						//	
		| (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) ///*!< Analog input specified by PSEL with 1/3 prescaling used as input for the conversion. 
		| (ADC_CONFIG_RES_8bit << ADC_CONFIG_RES_Pos);					//				!< 8bit ADC resolution. 
*/
    
	// enable ADC		
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;					  													// Bit 0 : ADC enable. 	
	

	NRF_ADC->TASKS_START = 1;							//Start ADC sampling

	// wait for conversion to end
	while (!NRF_ADC->EVENTS_END)
	{}
		
	//end 		
	NRF_ADC->EVENTS_END = 0;			
		
	//Save your ADC result
	original_bat_adc = NRF_ADC->RESULT;	
	
	//Use the STOP task to save current. Workaround for PAN_028 rev1.1 anomaly 1.
	NRF_ADC->TASKS_STOP = 1;

	// disable ADC		
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Disabled;
        
    //batt_adc_to_percent(original_bat_adc);	
    batt_adc_to_percent(original_bat_adc);      
}


uint8_t get_battery_value(){    
    return bat_percentage;    
}
uint8_t get_battery_in_volt(){    
    return original_bat_adc;    
}
