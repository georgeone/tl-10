#include <stdint.h>
// *****************************************************************************
// Typedef
// *****************************************************************************
typedef struct
{
	uint16_t	adc;
	uint16_t	percent;
} bat_table_t;


typedef struct
{
	uint16_t	adc;
	uint16_t	temp;
} temp_table_t;


// *****************************************************************************
// Function
// *****************************************************************************
void batt_adc_check(void);



uint8_t get_battery_value(void);
uint8_t get_battery_in_volt(void);  

