/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>



#include "nrf_drv_adc.h"
#include "nrf_log.h"


#include "led_softblink.h"
#include "sdk_errors.h"
#include "nrf_drv_clock.h"


#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "device_manager.h"
#include "pstorage.h"
#include "app_trace.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "sensorsim.h"
#include "nrf_gpio.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_custom_service.h"
#include "../Dfu/ble_dfu.h"
#include "../Dfu/dfu_app_handler.h"
#include "../Src/first_boot_src.h"
#include "common.h"
#include "my_p_storage_handler.h"
#include "SEGGER_RTT.h"
#include "ble_bas.h"
#include "app_pwm.h"
#include "nrf_delay.h"
#include "../Drv/adc.h"
#include "app_twi.h"
#include "app_util_platform.h"

//#define MY_DEBUGGING_MODE
#ifdef MY_DEBUGGING_MODE
    #define my_printf SEGGER_RTT_printf
#else 
    #define my_printf(...)
#endif

#define IS_SRVC_CHANGED_CHARACT_PRESENT  1                                          /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define CENTRAL_LINK_COUNT               0                                          /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT            1                                          /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define MANUFACTURER_NAME                "Technonia"                            			/**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                 300                                        /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS       0                                        /**< The advertising timeout in units of seconds. */

#define APP_TIMER_PRESCALER              0                                          /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE          8                                          /**< Size of timer operation queues. */
				
#define MIN_CONN_INTERVAL                MSEC_TO_UNITS(100, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL                MSEC_TO_UNITS(200, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                    0                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY    APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER)/**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT     3                                          /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                   1                                          /**< Perform bonding. */
#define SEC_PARAM_MITM                   0                                          /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                   0                                          /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS               0                                          /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES        BLE_GAP_IO_CAPS_NONE                       /**< No I/O capabilities. */
#define SEC_PARAM_OOB                    0                                          /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE           7                                          /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE           16                                         /**< Maximum encryption key size. */

#define DEAD_BEEF                        0xDEADBEEF                                 /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define DFU_REV_MAJOR                    0x00                                       /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                    0x01                                       /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                     ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */
#define APP_SERVICE_HANDLE_START         0x000C                                     /**< Handle of first application specific service when when service changed characteristic is present. */
#define BLE_HANDLE_MAX                   0xFFFF                                     /**< Max handle value in BLE. */



#define RTC_TIMER_INTERVAL                  APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)      // Interval (ticks). 1000ms
#define RESET_TIMER_INTERVAL                APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)      // Interval (ticks). 1000ms
#define DISCONNECT_TIMER_INTERVAL                APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)      // Interval (ticks). 1000ms
#define BUTTON_TIMER_PWR_INTERVAL	        APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)      // Interval (ticks). 50ms
#define BUTTON_TIMER_COIL_INTERVAL 						APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)	//// Interval (ticks). 50ms
#define ADC_TIMER_INTERVAL										APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)						/// Interval (ticks). 50ms
#define DOUBLE_PUSH_TIMER_INTERVAL						APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)	/// Interval (ticks). 50ms



//Servicee UUID
//====================================================================
#define BLE_CUSTOM_SERVICE_UUID      0x0000

#define PACKET_TYPE                     p_data[0]

#define INTERVAL_1						broadcast_setting_data[1]
#define INTERVAL_2						broadcast_setting_data[0]
#define TX_POWER              			broadcast_setting_data[2]
//#define ADC_BUFFER_SIZE 1  



static dm_application_instance_t         m_app_handle;                              /**< Application identifier allocated by device manager */

static uint16_t                          m_conn_handle = BLE_CONN_HANDLE_INVALID;   /**< Handle of the current connection. */


//service 
static ble_bas_t                            m_bas;      
static ble_custom_service_t                 m_setting_service;     
static ble_dfu_t                            m_dfus;                                    /**< Structure used to identify the DFU service. */



APP_TIMER_DEF(m_rtc_timer_id);   
APP_TIMER_DEF(m_reset_timer_id);   
APP_TIMER_DEF(m_button_timer_pwr_id);
APP_TIMER_DEF(m_button_timer_coil_id);
APP_TIMER_DEF(m_adc_timer_id);
APP_TIMER_DEF(m_button_double_push_timer_id);



static uint8_t device_mac_name[12]  =   {'T', 'L', '-', '1', '0','-',0x00, 0x00, 0x00, 0x00, 0x00, 0x00};   



                                   
//data buffer
static uint8_t                          broadcast_setting_data[16];        //data for setting advertising 

 
static uint8_t                          mac_addr_tx_buffer[8]={0x71};
static ble_gap_addr_t          			mac_addr_buffer;  					//buffer for getting device address (mac address) 

static uint8_t                          send_noti[4]={0x12};

static uint8_t                          send_off[4]={0x13};

static uint8_t				systme_status = false;
static uint8_t				coil_status = false;
static uint8_t				led_toggle_status =false;
static uint8_t				recently_powered = false;
static uint32_t				timer_pwr_tick		=0;
static uint32_t 			timer_coil_tick			=0;
static uint32_t			double_push_tick = 0;
static uint8_t 				service_buffer =  false;
static uint8_t 				coil_timer_start_helper = false;
static uint8_t				blink_timing = false;
static uint8_t				button_pushed_count = false;
static uint8_t 				just_double_pushed = false;
//static sensorsim_cfg_t   m_battery_sim_cfg;               /**< Battery Level sensor simulator configuration. */
//static sensorsim_state_t m_battery_sim_state; 

//static uint8_t				is_my_button_push 		=0;
static uint8_t				noti_send_status=0;
void power_manage(void);
static void advertising_init(void);
void conn_params_init(void);

static nrf_adc_value_t       adc_value; /**< ADC buffer. */
static nrf_drv_adc_channel_t m_channel_config = NRF_DRV_ADC_DEFAULT_CHANNEL(NRF_ADC_CONFIG_INPUT_7);

void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


static void advertising_stop(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);


}



/**@brief Function for preparing for system reset. 
 * 
 * @details This function implements @ref dfu_app_reset_prepare_t. It will be called by 
 *          @ref dfu_app_handler.c before entering the bootloader/DFU. 
 *This allows the current running application to shut down gracefully. 
*/



static void reset_prepare(void){   
    
    uint32_t err_code;    
    
    if(m_conn_handle != BLE_CONN_HANDLE_INVALID){
        // Disconnect from peer.        
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);        
        APP_ERROR_CHECK(err_code);        
 
    }    
    else{        
        // If not connected, the device will be advertising. Hence stop the advertising.        
        advertising_stop();    
    }    
    err_code = ble_conn_params_stop();    
    APP_ERROR_CHECK(err_code);    
    nrf_delay_ms(500);
}






/**@brief Function for counting RTC value. 
 */

static void adc_timeout_handler(void * p_context)
{
	UNUSED_PARAMETER(p_context);
	
				nrf_drv_adc_sample_convert(&m_channel_config,&adc_value);
				if(coil_status == true){
					if(adc_value> 475)
						nrf_gpio_pin_clear(COIL);
					else
						nrf_gpio_pin_set(COIL);
				}
					else
						nrf_gpio_pin_clear(COIL); 
			
	

}


static void double_push_timeout_handler(void * p_context)
{
	UNUSED_PARAMETER(p_context);
	uint32_t err_code;
	

	
	if(button_pushed_count >= 2 && double_push_tick <= 8 /*0.05sec*8 = 0.4sec*/){ 
		if ( true == service_buffer) {
					ble_custom_service_send_string_1(&m_setting_service, send_noti, 1);    
					service_buffer = false;
		}
		
		button_pushed_count = 0;
		//nrf_delay_ms(100); 	//Stablize while sending data.
		double_push_tick = 0;
		just_double_pushed = true;
		err_code = app_timer_stop(m_button_double_push_timer_id);
		APP_ERROR_CHECK(err_code);
	}
	
	if(double_push_tick > 8/*Case when 0.4sec is passed but failed to double push*/){
		button_pushed_count = 0;
		double_push_tick = 0;
		err_code = app_timer_stop(m_button_double_push_timer_id);
		APP_ERROR_CHECK(err_code);
		just_double_pushed = false;
	}
	
	double_push_tick++; //increases every 0.05sec
		
}
static void rtc_timeout_handler(void * p_context)
{
  UNUSED_PARAMETER(p_context);
	 
		
}

static void reset_timeout_handler(void * p_context)
{
    NVIC_SystemReset();
}

/**@brief Function for counting RTC value. 
 */
static void button_timeout_pwr_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);	 
	
		uint32_t err_code;    
		  

		if(timer_pwr_tick > 20){

				if(systme_status==false)// If the system was off and the button push continued for 1sec.
				{
					nrf_gpio_pin_clear(LED_PWR);	//LED ON
			
					err_code = ble_advertising_start(BLE_ADV_MODE_FAST);	//Advertising start
					APP_ERROR_CHECK(err_code);
					systme_status = true;
					recently_powered = true;
					err_code = app_timer_start(m_button_timer_coil_id, BUTTON_TIMER_COIL_INTERVAL, NULL);
					APP_ERROR_CHECK(err_code);	//Timer for coil starts -> Enable to activate coil
					err_code = app_timer_start(m_adc_timer_id, ADC_TIMER_INTERVAL, NULL); // Timer for adc starts -> Enable to get values from adc channel
					APP_ERROR_CHECK(err_code);
					
					err_code = app_timer_stop(m_button_timer_pwr_id);
					APP_ERROR_CHECK(err_code);
					
				}
				else{// If the system was on and the button push continued for 1sec.
				nrf_gpio_pin_set(LED_PWR); //LED OFF
				
				nrf_gpio_pin_clear(COIL);				//COIL OFF
				coil_status = false;
				ble_custom_service_send_string_1(&m_setting_service, send_off, 1);
					
				nrf_delay_ms(1500); //wait 1.5sec for device to send message

				advertising_stop();
				sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION); 
				
				err_code = app_timer_stop(m_button_timer_coil_id);
				APP_ERROR_CHECK(err_code);
				coil_timer_start_helper = false;
					
				err_code = app_timer_stop(m_adc_timer_id);
				APP_ERROR_CHECK(err_code);	
					
				systme_status=false; //all system disabled
				
				err_code = app_timer_stop(m_button_timer_pwr_id);
				APP_ERROR_CHECK(err_code);
				NVIC_SystemReset();
			}
			
			
		}
		else{
			if (systme_status == true)
				service_buffer = true;

		}  
			

		if( 0 == nrf_gpio_pin_read(BUTTON_1)){
			timer_pwr_tick++;
			
		}
		else{
			timer_pwr_tick=0;
		}

}


void button_timeout_coil_handler(void * p_context)
{

	
	if(coil_status == true) //if coil is powered
	{
		if(blink_timing == 0 || blink_timing == 1 || blink_timing == 2)
			nrf_gpio_pin_set(LED_PWR);
		else{
			nrf_gpio_pin_clear(LED_PWR);
			if(blink_timing == 4)
				blink_timing = 0;
		}
		blink_timing++;
	}
	
	if((timer_coil_tick > 9) && just_double_pushed == false ){

		if(led_toggle_status == true && coil_status == false)
		{
			nrf_gpio_pin_set(COIL);
			coil_status = true;
			
			
			
			timer_coil_tick = 0;
			led_toggle_status = false;
			coil_timer_start_helper = false;
		}
		else if(led_toggle_status == true && coil_status == true)
		{
			nrf_gpio_pin_clear(COIL); // COIL OFF
			coil_status = false;
			
			
			timer_coil_tick = 0;
			led_toggle_status = false;
			coil_timer_start_helper = false;
			nrf_gpio_pin_clear(LED_PWR);
		}
		
	}
	
	
	if(coil_timer_start_helper == true)
		timer_coil_tick++;
	else
		timer_coil_tick = 0;
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    uint32_t err_code;    
    // Initialize timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create timers.    
    err_code = app_timer_create(&m_rtc_timer_id, APP_TIMER_MODE_REPEATED, rtc_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
    err_code = app_timer_create(&m_reset_timer_id, APP_TIMER_MODE_SINGLE_SHOT, reset_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
		err_code = app_timer_create(&m_button_timer_pwr_id, APP_TIMER_MODE_REPEATED, button_timeout_pwr_handler);
    APP_ERROR_CHECK(err_code); 
	
		err_code = app_timer_create(&m_button_timer_coil_id, APP_TIMER_MODE_REPEATED, button_timeout_coil_handler);
    APP_ERROR_CHECK(err_code);
	
		err_code = app_timer_create(&m_adc_timer_id, APP_TIMER_MODE_REPEATED, adc_timeout_handler);
    APP_ERROR_CHECK(err_code);
	
		err_code = app_timer_create(&m_button_double_push_timer_id, APP_TIMER_MODE_REPEATED, double_push_timeout_handler);
		APP_ERROR_CHECK(err_code);
	

}


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;    

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    //get device_MAC address   
    sd_ble_gap_address_get(&mac_addr_buffer);
    
    //set device name 
    snprintf((char *)device_mac_name+(sizeof(device_mac_name)-6), 3, "%02X", mac_addr_buffer.addr[1]);   //length is n-1 
    snprintf((char *)device_mac_name+(sizeof(device_mac_name)-4), 3, "%02X", mac_addr_buffer.addr[0]);   //length is n-1 
    
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)device_mac_name,
                                          sizeof(device_mac_name)-1);  
										    
    APP_ERROR_CHECK(err_code);


    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the RTC Service events. 
 * @details This function will be called for RTC Service events which are passed to
 *          the application.
 *
 * @param[in]   p_custom_service   p_custom_service structure.
 * @param[in]   p_evt          Event received from the RTC Service.
 *
 */
void custom_service_data_handler(ble_custom_service_t * p_custom_service, uint8_t * p_data, uint16_t length)
{
    
    
	my_printf(0, "[p_data_index] : %d \n", p_data[0]);
	
    switch(PACKET_TYPE){
        
        
        case NOTI:                   
			nrf_gpio_pin_toggle(LED_PWR);          
            break;
           /* 
        case DEV_STATUS_REQ:          
            break;
            */
       
        default:
           
            break;
    }
 
}


/**@brief Function for initializing Battery Service.
 */
static void bas_init(void)
{
    uint32_t       err_code;
    ble_bas_init_t bas_init_obj;

    memset(&bas_init_obj, 0, sizeof(bas_init_obj));

    bas_init_obj.evt_handler          = NULL;
    bas_init_obj.p_report_ref         = NULL;
    bas_init_obj.initial_batt_level   = 100;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init_obj.battery_level_char_attr_md.read_perm);

    err_code = ble_bas_init(&m_bas, &bas_init_obj);
    APP_ERROR_CHECK(err_code);
    
}

/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
        
    uint32_t         err_code;    
    ble_custom_service_init_t   custom_service_init;   
    ble_dfu_init_t   dfus_init;
    
    bas_init();
    
    //custom service    
    memset(&custom_service_init, 0, sizeof(custom_service_init));
    custom_service_init.data_handler = custom_service_data_handler;	
    
    err_code = ble_custom_service_init(&m_setting_service, &custom_service_init);    
    APP_ERROR_CHECK(err_code);
    
    //dfu service  
    memset(&dfus_init, 0, sizeof(dfus_init));    
    dfus_init.evt_handler   = dfu_app_on_dfu_evt;    
    dfus_init.error_handler = NULL;    
    dfus_init.evt_handler   = dfu_app_on_dfu_evt;    
    dfus_init.revision      = DFU_REVISION;    
    
    err_code = ble_dfu_init(&m_dfus, &dfus_init);   
    APP_ERROR_CHECK(err_code);    
    dfu_app_reset_prepare_set(reset_prepare);  
    dfu_app_dm_appl_instance_set(m_app_handle);
    
	}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            
            break;
        case BLE_ADV_EVT_IDLE:
            //sleep_mode_enter();
            break;
        default:
            break;
    }
}


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    switch (p_ble_evt->header.evt_id)
            {
        case BLE_GAP_EVT_CONNECTED:            
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

			my_printf(0, "[Connected!!!]\n");
		   
            break;

        case BLE_GAP_EVT_DISCONNECTED:

            m_conn_handle = BLE_CONN_HANDLE_INVALID;

			my_printf(0, "[Disconnected!!!]\n");			
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    dm_ble_evt_handler(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    bsp_btn_ble_on_ble_evt(p_ble_evt);
    on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
    ble_custom_service_on_ble_evt(&m_setting_service, p_ble_evt);
    ble_dfu_on_ble_evt(&m_dfus, p_ble_evt);
    ble_bas_on_ble_evt(&m_bas, p_ble_evt); 
}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    //pstorage_sys_event_handler(sys_evt);
    ble_advertising_on_sys_evt(sys_evt);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;
    
		nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;

    
    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
    
    ble_enable_params_t ble_enable_params;
    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                    PERIPHERAL_LINK_COUNT,
                                                    &ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    ble_enable_params.gatts_enable_params.service_changed = 1;
    ble_enable_params.common_enable_params.vs_uuid_count = 2;
    
    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
	
void bsp_event_handler(bsp_event_t event)
{
    uint32_t err_code;
    
    switch (event)
    {
        case BSP_EVENT_DISCONNECT:

            break;

        case BSP_EVENT_WHITELIST_OFF:
            err_code = ble_advertising_restart_without_whitelist();
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;
            
        case BSP_EVENT_KEY_0:
			
					err_code = app_timer_start(m_button_timer_pwr_id, BUTTON_TIMER_PWR_INTERVAL, NULL);
					APP_ERROR_CHECK(err_code);
					
					if(systme_status == true){
						err_code = app_timer_start(m_button_double_push_timer_id, DOUBLE_PUSH_TIMER_INTERVAL, NULL);
						APP_ERROR_CHECK(err_code);
						button_pushed_count++;
						coil_timer_start_helper = true;
					}

					led_toggle_status = false;
				
					break;
				
			
		case BSP_EVENT_KEY_1:
			
			err_code = app_timer_stop(m_button_timer_pwr_id);
			APP_ERROR_CHECK(err_code);
			timer_pwr_tick=0;
			if(recently_powered == true)
			{
					recently_powered = false;
			}
			else if(recently_powered == false && systme_status == true)
			{
				led_toggle_status = true;
			}
			
            break;


        default:
            break;
		
    }
}


/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           ret_code_t        event_result)
{
    APP_ERROR_CHECK(event_result);

#ifdef BLE_DFU_APP_SUPPORT
    if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }
#endif // BLE_DFU_APP_SUPPORT

    return NRF_SUCCESS;
}


/**@brief Function for the Device Manager initialization.
 */
static void device_manager_init()
{
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = NULL};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.lesc         = SEC_PARAM_LESC;
    register_param.sec_param.keypress     = SEC_PARAM_KEYPRESS;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t scanrsp;
    
     ble_uuid_t adv_uuids[] = {{BLE_CUSTOM_SERVICE_UUID, m_setting_service.uuid_type}};
     
    // Build advertising data struct to pass into @ref ble_advertising_init.
    memset(&advdata, 0, sizeof(advdata));    
     
    advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    advdata.uuids_complete.p_uuids  = adv_uuids;
     
    advdata.include_appearance      = false;
    advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
       
    memset(&scanrsp, 0, sizeof(scanrsp));
    scanrsp.name_type = BLE_ADVDATA_FULL_NAME;     
     
    ble_adv_modes_config_t options = {0};
    options.ble_adv_fast_enabled  = BLE_ADV_FAST_ENABLED;
    options.ble_adv_fast_interval = 0x0020;
    options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;
  
    err_code = ble_advertising_init(&advdata, &scanrsp, &options, on_adv_evt, NULL);
    APP_ERROR_CHECK(err_code);
    
    sd_ble_gap_tx_power_set(TX_POWER);
}


/**@brief Function for initializing buttons and leds.
 */
static void buttons_leds_init()
{

    uint32_t err_code = bsp_init(/*BSP_INIT_LED |*/ BSP_INIT_BUTTONS
                                 ,APP_TIMER_TICKS(100, APP_TIMER_PRESCALER), 
                                 bsp_event_handler);

    APP_ERROR_CHECK(err_code);
		


    err_code = bsp_event_to_button_action_assign(0, BSP_BUTTON_ACTION_PUSH, BSP_EVENT_KEY_0); //what the 0 means for
    APP_ERROR_CHECK(err_code);	


		err_code = bsp_event_to_button_action_assign(0, BSP_BUTTON_ACTION_RELEASE, BSP_EVENT_KEY_1);
    APP_ERROR_CHECK(err_code);	

}
/**@brief Function for initializing pins. 
 */

static void pin_init(void)
{    

	
    nrf_gpio_cfg_output(LED_PWR);
		nrf_gpio_cfg_output(COIL);
  

	nrf_gpio_pin_set(LED_PWR);
	nrf_gpio_pin_clear(COIL);

	nrf_gpio_cfg_input(BUTTON_1, NRF_GPIO_PIN_PULLUP);
	
	

	
	

}



/**@brief Function for the Power manager.
 */
void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for swapping mac address start<->end
 */
void swap_mac_addr(void)
{
	uint8_t swap_buffer;	
	
	swap_buffer=mac_addr_buffer.addr[0];	
	mac_addr_buffer.addr[0]=mac_addr_buffer.addr[5];
	mac_addr_buffer.addr[5]=swap_buffer;	
		
	swap_buffer=mac_addr_buffer.addr[1];	
	mac_addr_buffer.addr[1]=mac_addr_buffer.addr[4];
	mac_addr_buffer.addr[4]=swap_buffer;	
		
	swap_buffer=mac_addr_buffer.addr[2];	
	mac_addr_buffer.addr[2]=mac_addr_buffer.addr[3];
	mac_addr_buffer.addr[3]=swap_buffer;
	
	memcpy(mac_addr_tx_buffer+sizeof(uint8_t)*1, mac_addr_buffer.addr, sizeof(uint8_t)*6);
	
}


	
	


/**@brief Function for application main entry.
 */
int main(void)
{
		
    uint32_t err_code;
     
		
    
    // Initialize.
    pin_init(); 
    timers_init();   
    buttons_leds_init();
    
    ble_stack_init();
    device_manager_init();

    gap_params_init(); 
    services_init();    
    advertising_init();   
    conn_params_init();
		//		adc_config();
    //RTC timer start 
    err_code = app_timer_start(m_rtc_timer_id, RTC_TIMER_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
		
		swap_mac_addr();
		
		
		
		
    // Enter main loop.
		
    for (;;)
    {
        power_manage();
    }
}

/**
 * @}
 * ROM, RAM 사용
* ROM: 전체 256kB(0x040000) = "SD: 0x1B000" + "app size: 0x???" + "Free (for DFU): 0x???" : + "App data: 0x????" + "DFU bootloader : 0x04000" 
* RAM: 전체  16KB(0x4000)  = "SD: 0x2000(최소 0x13c8인데, ble stack (multi link), attrubute, security, service 개수에따라 다름" + "app사용:0x1A00 "  + "call stack 0x600" 
* 따라서, ROM은 1B000 부터 size는 0x20000, RAM은0x20002000부터 size는 0x1A00으로 설정 함. 
* 추가. BLE stack에 따른 RAM 사용은 components\softdevice\common\soft_device_handler\app_ram_base.h 를 참고하면 됨. 
 */
