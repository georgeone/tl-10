/* Copyright (c) 2014 Technonia. All Rights Reserved.
*/



#include <stdint.h>




void my_p_storage_init(int i_block_size, int i_block_count);


void storage_load(int ident, uint8_t * dest, int size, int offset);
void storage_save(int ident, uint8_t * source, int size, int offset);
void storage_clear(int i_block_size, int i_block_count);
void storage_update(uint16_t ident, uint8_t * source, int size, int offset);









