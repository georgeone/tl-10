


#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "nordic_common.h"
#include <string.h>


#define GET_FIRM_VER                        0x01

#define NOTI			                    0x11
/*
#define GET_ADV_INTERVAL                    0x12

#define SET_TX_POWER                        0x21
#define GET_TX_POWER                        0x22

#define GET_USAGE_HISTORY                   0x31
#define USAGE_HISTORY_RSP                   0x32

#define ERASE_ALL_USAGE_HISTORY             0x41

#define DEV_STATUS_REQ                      0x51

#define MEASURE_LDI_VAL                     0x61

#define MAC_ADDR_REQ                        0x71

#define ADV_RESET		                    0x81

*/

#define BLE_WRITE_CHARACTERISTIC                0x0001
#define BLE_NOTIFY_1_CHARACTERISTIC             0x0002     
#define BLE_READ_CHARACTERISTIC                 0x0003     
                     
        
#define BLE_CUSTOM_MAX_DATA_LEN            (GATT_MTU_SIZE_DEFAULT - 3)  /**< Maximum length of data (in bytes) that can be transmitted. */

#define BLE_CUSTOM_MAX_RX_CHAR_LEN         BLE_CUSTOM_MAX_DATA_LEN         /**< Maximum length of the RX Characteristic (in bytes). */
#define BLE_CUSTOM_MAX_TX_CHAR_LEN         20                           /**< Maximum length of the TX Characteristic (in bytes). */

// Forward declaration of the ble_custom_t type.
typedef struct ble_custom_service_s ble_custom_service_t;

/**@brief Custom Service event handler type. */
typedef void(*ble_custom_service_data_handler_t) (ble_custom_service_t * p_custom_service, uint8_t * data, uint16_t length);



/**@brief     Custom service initialization structure.
*
* @details    This structure contains the initialization information for the Custom Service. The
*             application needs to fill this structure and pass it to the Custom Service using the
*             @ref ble_custom_service_init function.
*/
typedef struct
{
	ble_custom_service_data_handler_t        data_handler;                           /**< Event handler to be called for handling events in the Custom Service. */
} ble_custom_service_init_t;



/**@brief   Custom service structure.
*
* @details This structure contains status information related to the service.
*/
typedef struct ble_custom_service_s
{
	uint8_t                      uuid_type;                         /**< UUID type assigned for OID Service by the S110 SoftDevice. */
	uint16_t                     conn_handle;                       /**< Handle of the current connection (as provided by the S110 SoftDevice). This will be BLE_CONN_HANDLE_INVALID when not in a connection. */
	uint16_t                     service_handle;                    /**< Handle of OID Service (as provided by the S110 SoftDevice). */
	ble_gatts_char_handles_t     tx_handles;                        /**< Handles related to the TX characteristic. (as provided by the S110 SoftDevice)*/
	ble_gatts_char_handles_t     rx_1_handles;                        /**< Handles related to the RX characteristic. (as provided by the S110 SoftDevice)*/
	ble_custom_service_data_handler_t       data_handler;                      /**< The event handler to be called when an event is to be sent to the application.*/
	bool                         is_notification_enabled;           /**< Variable to indicate if the peer has enabled notification of the RX characteristic.*/
} ble_setting_service_t;


/**@brief      Function for initializing the Custom service.
*
* @param[out] p_custom_Service        Custom service structure. This structure will have to be
*                          supplied by the application. It will be initialized by this function,
*                          and will later be used to identify the service instance.
* @param[in]  p_oid_init   Information needed to initialize the service.
*
* @return     NRF_SUCCESS if the Setting service and its characteristics were successfully added to the
*             S110 SoftDevice. Otherwise an error code.
*             This function returns NRF_ERROR_NULL if the value of evt_handler in p_custom_init
*             structure provided is NULL or if the pointers supplied as input are NULL.
*/
uint32_t ble_custom_service_init(ble_custom_service_t * p_custom_service, const ble_custom_service_init_t * p_custom_service_init);


/**@brief      Function for handling a BLE event.
*
* @details    The Custom service expects the application to call this function each time an event
*             is received from the S110 SoftDevice. This function processes the event, if it is
*             relevant for the Setting service and calls the Setting event handler of the application if
*             necessary.
*
* @param[in]  p_custom_service        Pointer to the oid service structure.
* @param[in]  p_ble_evt    Pointer to the event received from S110 SoftDevice.
*/
void ble_custom_service_on_ble_evt(ble_custom_service_t * p_custom_service, ble_evt_t * p_ble_evt);



/**@brief       Function for sending a string to the peer.
 *
 * @details     This function will send the input string as a RX characteristic notification to the
 *              peer.
  *
 * @param[in]   p_custom_service,          Pointer to the Custom Service structure.
 * @param[in]   string         String to be sent.
 * @param[in]   length         Length of string.
 *
 * @return      NRF_SUCCESS if the DFU Service has successfully requested the S110 SoftDevice to
 *              send the notification. Otherwise an error code.
 *              This function returns NRF_ERROR_INVALID_STATE if the device is not connected to a
 *              peer or if the notification of the RX characteristic was not enabled by the peer.
 *              It returns NRF_ERROR_NULL if the pointer p_nus is NULL.
 */
uint32_t ble_custom_service_send_string_1(ble_custom_service_t * p_custom_service, uint8_t * string, uint16_t length);
uint32_t ble_custom_service_send_string_2(ble_custom_service_t * p_custom_service, uint8_t * string, uint16_t length);
uint32_t ble_custom_service_send_adc_1(ble_custom_service_t * p_custom_service, int16_t * adc_value, uint16_t length);


//extern void ble_setting_on_ble_evt(ble_setting_t * p_setting, ble_evt_t * p_ble_evt);


void custom_service_data_handler(ble_custom_service_t * p_custom_service, uint8_t * p_data, uint16_t length);
void gap_params_init(void);







