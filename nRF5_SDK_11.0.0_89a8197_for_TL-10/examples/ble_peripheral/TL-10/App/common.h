#include <stdint.h>
#include <string.h>



//Pstorage information 
#define BLOCK_SIZE              0x10    // 16 byte, block size should be multiples of 4. 
#define BLOCK_COUNT            640    //10 blocks 

#define SETTING_BLOCK           0   



//General 
//====================================================================
#define FALSE   			0
#define TRUE    			1

#define LOW					0
#define HIGH				1

#define OFF					0
#define ON					1


//#define GETTING_DATA                     0x20
//#define SETTING_DATA                     0x21


/**@brief RTC data.*/
typedef struct
{   
    uint8_t             day;     //mon(0), tue(1), wed(2), thu(3), fri(4), sat(5), sun(6) 
    uint8_t             hour;    //0~23 
    uint8_t             minute;  //0~59 
    uint8_t             second;  //0~59 
} rtc_data_t;

