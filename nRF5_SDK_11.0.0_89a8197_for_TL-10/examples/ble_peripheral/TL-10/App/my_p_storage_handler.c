/* Copyright (c) 2014 Technonia. All Rights Reserved.
*/


#include "my_p_storage_handler.h"
#include "pstorage.h"
#include "app_error.h"


static pstorage_handle_t p_id;   //pstorage base id
static pstorage_handle_t p_block_id;   //pstorage block id 
static int  pstorage_wait_flag = 0;

extern void power_manage(void);
//wait p_storage operation
void wait_till_op(){
    
	while (pstorage_wait_flag)	{	
        power_manage();	
	}
}


static void pstorage_cb_handler(pstorage_handle_t  * handle, uint8_t   op_code, uint32_t   result, uint8_t   * p_data, uint32_t  data_len){
	//handle: callback이 받는 block  
	//op_code: 
	//result about flash access.  Success return  NRF_SUCCESS 
	//p_data and data_len are info about source data. 
    pstorage_wait_flag =0;
    
	switch (op_code)	{
        case PSTORAGE_STORE_OP_CODE:
            //p_ready = 1;
            break;

        case PSTORAGE_LOAD_OP_CODE:
            //p_ready = 1;
            break;

        case PSTORAGE_CLEAR_OP_CODE:
           //p_ready = 1;
            break;

        case PSTORAGE_UPDATE_OP_CODE:
            //p_ready = 1;
            break;
        //error 
        default:   
            break;
	}
}

//Warning.
//Do not call p_storage_ready twice or more than one
void my_p_storage_init(int i_block_size, int i_block_count){

	uint32_t err_code;
	pstorage_module_param_t p_param;

	p_param.block_size = i_block_size;  //block size in load, store, update should be multuples of 4.  
	p_param.block_count = i_block_count;   
	p_param.cb = pstorage_cb_handler;

	//err_code = pstorage_init();
	//APP_ERROR_CHECK(err_code);

	err_code = pstorage_register(&p_param, &p_id);    
	APP_ERROR_CHECK(err_code);    
   
}


void storage_load(int ident, uint8_t * dest, int size, int offset){
	uint32_t err_code;
   
	err_code = pstorage_block_identifier_get(&p_id, ident, &p_block_id);    
	APP_ERROR_CHECK(err_code);

    pstorage_wait_flag = 1;
	err_code = pstorage_load(dest, &p_block_id, size, offset);	//3rd parameter is data size. it should be multiples of 4. 	
	APP_ERROR_CHECK(err_code);
    
	wait_till_op();
 
}

void storage_save(int ident, uint8_t * source, int size, int offset){
	uint32_t err_code;

	err_code = pstorage_block_identifier_get(&p_id, ident, &p_block_id);
	APP_ERROR_CHECK(err_code);
    
	err_code = pstorage_store(&p_block_id, source, size, offset);  //3rd parameter is data size. it should be multiples of 4. 	
    APP_ERROR_CHECK(err_code);

    
	wait_till_op();

}

void storage_clear(int i_block_size, int i_block_count){
	uint32_t err_code;

	err_code = pstorage_clear(&p_id, (i_block_size*i_block_count));	
	APP_ERROR_CHECK(err_code);

	wait_till_op();
  

}


void storage_update(uint16_t ident, uint8_t * source, int size, int offset){
	uint32_t err_code;

	err_code = pstorage_block_identifier_get(&p_id, ident, &p_block_id);
	APP_ERROR_CHECK(err_code);	

	err_code = pstorage_update(&p_block_id, source, size, offset);  //3rd parameter is data size. it should be multiples of 4. 							
	APP_ERROR_CHECK(err_code);

    wait_till_op();
}


