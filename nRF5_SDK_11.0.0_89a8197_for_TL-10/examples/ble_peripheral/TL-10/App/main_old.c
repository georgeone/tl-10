/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "device_manager.h"
#include "pstorage.h"
#include "app_trace.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "sensorsim.h"
#include "nrf_gpio.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_custom_service.h"
#include "../Dfu/ble_dfu.h"
#include "../Dfu/dfu_app_handler.h"
#include "../Src/first_boot_src.h"
#include "common.h"
#include "my_p_storage_handler.h"
#include "SEGGER_RTT.h"
#include "ble_bas.h"
#include "app_pwm.h"
#include "nrf_delay.h"
#include "../Drv/adc.h"

///////////////////////////////////////////////////////////////////////////////////////
//#define MY_DEBUGGING_MODE
#ifdef MY_DEBUGGING_MODE
    #define my_printf SEGGER_RTT_printf
#else 
    #define my_printf(...)
#endif
///////////////////////////////////////////////////////////////////////////////////////

#define IS_SRVC_CHANGED_CHARACT_PRESENT  1                                          /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define CENTRAL_LINK_COUNT               0                                          /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT            1                                          /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define MANUFACTURER_NAME                "doubleH"                            			/**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                 300                                        /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS       0                                        /**< The advertising timeout in units of seconds. */

#define APP_TIMER_PRESCALER              0                                          /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE          8                                          /**< Size of timer operation queues. */
				
#define MIN_CONN_INTERVAL                MSEC_TO_UNITS(100, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL                MSEC_TO_UNITS(200, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                    0                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY    APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER)/**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT     3                                          /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                   1                                          /**< Perform bonding. */
#define SEC_PARAM_MITM                   0                                          /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                   0                                          /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS               0                                          /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES        BLE_GAP_IO_CAPS_NONE                       /**< No I/O capabilities. */
#define SEC_PARAM_OOB                    0                                          /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE           7                                          /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE           16                                         /**< Maximum encryption key size. */

#define DEAD_BEEF                        0xDEADBEEF                                 /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define DFU_REV_MAJOR                    0x00                                       /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                    0x01                                       /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                     ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */
#define APP_SERVICE_HANDLE_START         0x000C                                     /**< Handle of first application specific service when when service changed characteristic is present. */
#define BLE_HANDLE_MAX                   0xFFFF                                     /**< Max handle value in BLE. */

//STATIC_ASSERT(IS_SRVC_CHANGED_CHARACT_PRESENT);                                     /** When having DFU Service support in application the Service Changed Characteristic should always be present. */


#define STORAGE_SAVE_CODE               1
#define STORAGE_UPDATE_CODE             2

#define RTC_TIMER_INTERVAL                  APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)      // interval (ticks). 1000ms
#define USING_TIMER_INTERVAL                APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)      // interval (ticks). 1000ms
#define POWER_ON_TIMER_INTERVAL             APP_TIMER_TICKS(1, APP_TIMER_PRESCALER)         // interval (ticks). 1ms
#define MOTOR_MODE_CHANGE_TIMER_INTERVAL    APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)      // interval (ticks). 2000ms
#define IR_LED_TIMER_INTERVAL               APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)       // interval (ticks). 50ms
#define LDI_TIMER_INTERVAL                  APP_TIMER_TICKS(3, APP_TIMER_PRESCALER)       // interval (ticks). 3ms
#define USAGE_HISTORY_SEND_INTERVAL         APP_TIMER_TICKS(200, APP_TIMER_PRESCALER)       // interval (ticks). 200ms
#define RESET_TIMER_INTERVAL                APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)      // interval (ticks). 1000ms
#define DISCONNECTION_TIMER_INTERVAL        APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)      // interval (ticks). 1000ms
#define ENDING_VIB_TIMER_INTERVAL        		APP_TIMER_TICKS(235, APP_TIMER_PRESCALER)      // interval (ticks). 200ms
#define DELAY_TIMER_INTERVAL        				APP_TIMER_TICKS(700, APP_TIMER_PRESCALER)      // interval (ticks). 500ms
#define FULL_DETECT_PWR_LED_BLINK_DELAY    	APP_TIMER_TICKS(350, APP_TIMER_PRESCALER)      // interval (ticks). 500ms
#define CHARGING_PWR_LED_BLINK_DELAY    		APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER)      // interval (ticks). 1000ms

#define LUMI_USING_TIME                     1800

#define LOW_BATT_NOTI_ADC                   0x4F   //(3.4V) 
#define LOW_BATT_ADC                        0x4C   //(3.3V)
#define CUT_OFF_BATT_ADC                    0x4A   //(3.2V) 
#define ALMOST_FULL_BATT_ADC								0x59

#define LDI_AUTO_MODE                       0x01
#define LDI_MANUAL_MODE                     0x02

#define CHARGING_DELAY											600

//Servicee UUID 
//====================================================================
#define BLE_CUSTOM_SERVICE_UUID      0x0000

#define PACKET_TYPE                     p_data[0]

#define INTERVAL_1						broadcast_setting_data[1]
#define INTERVAL_2						broadcast_setting_data[0]
#define TX_POWER              			broadcast_setting_data[2]

// PWM control value
//========================================================================================
static volatile bool pwm_1_ready_flag;            // A flag indicating PWM status.
static volatile bool pwm_2_ready_flag;            // A flag indicating PWM status.

const static app_pwm_duty_t pwm_value[52] = {      
    0, 1, 2, 2, 3, 3, 4, 4, 5, 5,
    6, 6, 7, 9,11,13,15,18,20,23,
   25,29,31,34,37,40,43,47,50,53,
   56,59,62,65,68,71,74,77,80,82,
   84,86,89,90,92,94,95,96,97,100  
};      

const static uint8_t power_led_value[48] = {
    20,18,17,16,15,14,13,12,11,10,  //max value is 20. (더 큰 값을 넣으면 LED 불빛이 깜박이는 것이 눈으로 보임) 
     9, 9, 8, 8, 8, 7, 7, 7, 6, 6,
     6, 6, 5, 5, 5, 5, 4, 4, 4, 4,
     3, 3, 3, 2, 2, 2, 2, 2, 2, 2,
     2, 2, 2, 2, 2, 1, 1, 1
};           



static dm_application_instance_t         m_app_handle;                              /**< Application identifier allocated by device manager */

static uint16_t                          m_conn_handle = BLE_CONN_HANDLE_INVALID;   /**< Handle of the current connection. */


//service 
static ble_bas_t                            m_bas;      
static ble_custom_service_t                 m_setting_service;     
static ble_dfu_t                            m_dfus;                                    /**< Structure used to identify the DFU service. */


//static pstorage_handle_t p_id;   //pstorage base id
//static pstorage_handle_t p_block_id;   //pstorage block id 



//static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}}; /**< Universally unique service identifiers. */

APP_TIMER_DEF(m_rtc_timer_id);   
APP_TIMER_DEF(m_reset_timer_id);   
APP_TIMER_DEF(m_motor_mode_timer_id);   
APP_TIMER_DEF(m_power_on_timer_id);   
APP_TIMER_DEF(m_ir_led_on_timer_id);   
APP_TIMER_DEF(m_ldi_measure_timer_id);   
APP_TIMER_DEF(m_using_timer_id);   
APP_TIMER_DEF(m_usage_history_send_timer_id);   
APP_TIMER_DEF(m_disconnection_timer_id);   
APP_TIMER_DEF(m_ending_vib_timer_id);   
APP_TIMER_DEF(m_delay_timer_id);   
APP_TIMER_DEF(m_full_detect_pwr_led_blinking_id);   
APP_TIMER_DEF(m_charging_pwr_led_blinking_id);   


APP_PWM_INSTANCE(PWM1,1);                   // Create the instance "PWM1" using TIMER1.
APP_PWM_INSTANCE(PWM2,2);                   // Create the instance "PWM2" using TIMER2.


static uint8_t device_mac_name[12]  =   {'L', 'u', 'm', 'i', '-', 'S', '-', 0x00, 0x00, 0x00, 0x00};   
//static uint8_t device_mac_name[14]  =   {'L', 'u', 'm', 'i', '-', 'S', '1','1','-', 0x00, 0x00, 0x00, 0x00};   
static uint8_t firmware_version[4]  =   {GET_FIRM_VER,1,0,6};
static uint8_t err_invalid_param[4] =   {0x00,0xfe};

static uint8_t                          first_boot_flag;   
                                   
//data buffer
static uint8_t                          broadcast_setting_data[16];        //data for setting advertising 
static uint8_t                          resp[4];
static uint8_t                          send_buffer[20];
static uint8_t                          recv_buffer[20];   
static uint8_t                          usage_history_send_buffer[20];
static uint8_t                          usage_history_end[20]={0x31,0xf1,0xf2,0xf3,0xf4};
static uint8_t                          mac_addr_tx_buffer[8]={0x71};
static ble_gap_addr_t          					mac_addr_buffer;  					//buffer for getting device address (mac address) 
int32_t                                 ldi_temp_value;
static uint32_t                         unix_time_stamp;    


uint8_t                                 battery_value;
uint8_t                                 usage_history_over_flow_flag=0;

//mode
uint8_t                                 system_on=FALSE;
uint8_t                                 mode=0;
uint8_t                                 motor_toggle=0;
uint32_t                                using_time=0;
uint32_t                                p_storage_index;
uint8_t                                 using_status=0x02;
uint8_t                                 disconnection_tick=0;
uint8_t                                 user_terminated=0;
uint8_t																	ending_vib_cnt=0;

uint8_t 																full_detect_pwr_led_blinking_cnt=0;
uint8_t 																charging_tick=0;
//LDI service 
static uint8_t                          usage_history[20];
uint8_t                                 dev_status[20];
uint8_t                                 current_userID[4]={0,0,0,0};
uint8_t                                 ldi_type=LDI_MANUAL_MODE;
uint16_t                                ldi_value;

uint32_t                                start_time;    
uint32_t                                end_time;    
uint32_t 																charging_time;
uint8_t                                 ldi_m_cnt=1;
uint8_t                                 ldi_m_interval=1;


void power_manage(void);
static void advertising_init(void);
void setting_data_update(void);
void default_data_save(int);
void cut_off_volt_check(void);
void mode_change(void);
void using_timer_stop(void);
void end_vib_start(void);
void conn_params_init(void);

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for terminate using of Lumidiet Belt 
 *
 * @details to notice end of using, buzz the device. 
 */
void using_terminate(void){
			
	// set the start time when the start time is not initiated with RTC value (Unix time stamp)
	if(start_time <  946684800 ){
		start_time = unix_time_stamp - using_time;		
	}

	
	using_timer_stop();      
	
	//motor off 
	mode=0;      
	mode_change();
	
	
	//mode change timer start 
    app_timer_start(m_delay_timer_id, DELAY_TIMER_INTERVAL, NULL);      
	
	
}
static void advertising_stop(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);


}


void pwm_1_ready_callback(uint32_t pwm_id)    // PWM callback function
{    
    pwm_1_ready_flag = true;    
}
void pwm_2_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    pwm_2_ready_flag = true;   
}

void usage_history_update(){
    
    static uint8_t p_storage_index_buffer[4]; 
    uint8_t update_index=0;
    uint8_t test_i=0;
    uint8_t test_t=0;
    static uint8_t                          test_pstorage_buffer[20];

    memset(p_storage_index_buffer, 0, sizeof(p_storage_index_buffer));
    memset(usage_history, 0, sizeof(usage_history));
    
    memcpy(p_storage_index_buffer, &p_storage_index, sizeof(p_storage_index));

    storage_update(SETTING_BLOCK, p_storage_index_buffer, 4, 12);		

    //usage history update       
    memcpy(usage_history+sizeof(uint8_t)*(update_index),&current_userID,sizeof(current_userID));        
    update_index+=sizeof(current_userID);        
    my_printf(0, "[User Id]:%02x%02x%02x%02x \n", current_userID[0],current_userID[1],current_userID[2],current_userID[3]);
    
    if( 1 == user_terminated){
        usage_history[update_index]=(ldi_type|0x80);          
    }
    else{
        usage_history[update_index]=ldi_type;       
    }
    
     
    update_index++;     
    
    usage_history[update_index]=mode;        
    update_index++;        

    memcpy(usage_history+sizeof(uint8_t)*(update_index),&ldi_value,sizeof(ldi_value));            
    update_index+=+sizeof(ldi_value);            
    

	memcpy(usage_history+sizeof(uint8_t)*(update_index),&start_time,sizeof(unix_time_stamp));            
    update_index+=+sizeof(unix_time_stamp);            
   
	
    memcpy(usage_history+sizeof(uint8_t)*(update_index),&end_time,sizeof(unix_time_stamp));

    storage_save(p_storage_index, usage_history, 16, 0);       
    my_printf(0,"[P_STORAGE_INDEX]: %d\n",p_storage_index);
	
	////////////////////////////////////////////////
	my_printf(0,"[save usage history]:");
	for(test_t=0; test_t< BLOCK_SIZE ; test_t++){
		my_printf(0,"%02x",usage_history[test_t]);
	}
	
	my_printf(0,"\n");        
            
    ///////////////////////////////////////////////////////////////
    ///////////log/////////////////////////////////////////    
    
       
    for( test_i=1 ; test_i<=p_storage_index; test_i++){        
        
        storage_load(test_i, test_pstorage_buffer, 16, 0); 
        
        my_printf(0,"[Usage History]:");
        for(test_t=0; test_t< BLOCK_SIZE ; test_t++){
            my_printf(0,"%02x",test_pstorage_buffer[test_t]);
        }
        my_printf(0,"\n");        
    }

    
   ////////////////////////////////////////////////////////
      
    if(m_conn_handle != BLE_CONN_HANDLE_INVALID){       
        // Disconnect from peer.      
        //app_timer_start(m_disconnection_timer_id, DISCONNECTION_TIMER_INTERVAL, NULL);
        //usage_history
        usage_history_send_buffer[0]= GET_USAGE_HISTORY;        
        memcpy(usage_history_send_buffer + sizeof(uint8_t)*1, usage_history, 16);
        ble_custom_service_send_string_1(&m_setting_service, usage_history_send_buffer, 17);
		
		my_printf(0,"[Send data]:");
        for(test_t=0; test_t< 17 ; test_t++){
            my_printf(0,"%02x",usage_history_send_buffer[test_t]);
        }
        my_printf(0,"\n");
    }    

    p_storage_index++;
    
    user_terminated=0;

    
    if( p_storage_index == BLOCK_COUNT){
        p_storage_index=1;
        usage_history_over_flow_flag=1;
    }
    
  
}


void power_on_led_start()
{        
    uint32_t err_code;       
  
    err_code = app_timer_start(m_power_on_timer_id, POWER_ON_TIMER_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
      
}


void belt_led_start()
{        
    uint32_t err_code;
    
    app_pwm_enable(&PWM1);      
  
    err_code = app_timer_start(m_ir_led_on_timer_id, IR_LED_TIMER_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
      
}

void ldi_measure_start()
{        
    uint32_t err_code;
    
    app_pwm_enable(&PWM2);      
  
    err_code = app_timer_start(m_ldi_measure_timer_id, LDI_TIMER_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
    
    while (app_pwm_channel_duty_set(&PWM2, 0, 50) == NRF_ERROR_BUSY);    
          
}

void ldi_measure_stop()
{        
    uint32_t err_code;
    
    while (app_pwm_channel_duty_set(&PWM2, 0, 100) == NRF_ERROR_BUSY);    
    
    app_pwm_disable(&PWM2);      
  
    err_code = app_timer_stop(m_ldi_measure_timer_id);
    APP_ERROR_CHECK(err_code);
    
}


void using_timer_start()
{        
    uint32_t err_code;
     
    err_code = app_timer_start(m_using_timer_id, USING_TIMER_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
              
    start_time = unix_time_stamp;    
}

void using_timer_stop()
{           
    using_time=0;   
    
    app_timer_stop(m_using_timer_id);    
    end_time = unix_time_stamp;      
        
    //LED off 
    while (app_pwm_channel_duty_set(&PWM1, 0,pwm_value[0]) == NRF_ERROR_BUSY);    
    while (app_pwm_channel_duty_set(&PWM1, 1,pwm_value[0]) == NRF_ERROR_BUSY);
    
    app_pwm_disable(&PWM1);     
     
    nrf_gpio_pin_clear(LED_IR);
    nrf_gpio_pin_clear(LED_RED);
    nrf_gpio_pin_clear(LED_PWR);
    
    system_on=FALSE;
    
     my_printf(0, "[notice] entering history update \n");
    
    //pstorage update 
    usage_history_update();

    
   if(m_conn_handle != BLE_CONN_HANDLE_INVALID){       
        // Disconnect from peer.      
        app_timer_start(m_disconnection_timer_id, DISCONNECTION_TIMER_INTERVAL, NULL);
    }    
    else{        
        // If not connected, the device will be advertising. Hence stop the advertising.       
         advertising_stop();    
    }    

    
}

void end_vib_start(){
	uint32_t err_code;
     
    err_code = app_timer_start(m_ending_vib_timer_id, ENDING_VIB_TIMER_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);

	
}

/**@brief Function for preparing for system reset. 
 * 
 * @details This function implements @ref dfu_app_reset_prepare_t. It will be called by 
 *          @ref dfu_app_handler.c before entering the bootloader/DFU. 
 *This allows the current running application to shut down gracefully. 
*/
static void reset_prepare(void){   
    
    uint32_t err_code;    
    
    if(m_conn_handle != BLE_CONN_HANDLE_INVALID){
        // Disconnect from peer.        
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);        
        APP_ERROR_CHECK(err_code);        
 
    }    
    else{        
        // If not connected, the device will be advertising. Hence stop the advertising.        
        advertising_stop();    
    }    
    err_code = ble_conn_params_stop();    
    APP_ERROR_CHECK(err_code);    
    nrf_delay_ms(500);
}


/**@brief Function for performing a battery measurement, and update the Battery Level characteristic in the Battery Service.
 */
static void battery_level_update(void)
{
    uint32_t err_code;
    
    batt_adc_check();  
    
    battery_value=get_battery_value();  

    //battery_value=get_battery_in_volt();  
    /*
    uint8_t testsss[2];    
    testsss[0]=get_battery_in_volt();  
    
    ble_custom_service_send_string_1(&m_setting_service, testsss, 1);      
    */
	
    err_code = ble_bas_battery_level_update(&m_bas, battery_value);
    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&        
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
    )
    {
        APP_ERROR_HANDLER(err_code);
    }
}

static void charging_timeout_handler(void * p_context)
{

	if(charging_tick == 222){
		charging_tick=0;		
	}
	charging_tick++;
	
	if(1 == charging_tick%2){
			nrf_gpio_pin_set(LED_PWR);  
	}
	else{
		nrf_gpio_pin_clear(LED_PWR);  
	}
	
}
	


/**@brief Function for counting RTC value. 
 */
static void rtc_timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);	  
    static uint8_t low_batt_led_toggle=0;
    unix_time_stamp++;    

		battery_level_update();   //temp 
        
		
    //my_printf(0, "[Unix Time Stamp]: %d \n", unix_time_stamp);
		
		//완충이면, 
		if(0 == nrf_gpio_pin_read(FULL_DET) ) {
				nrf_gpio_pin_clear(LED_FULL);
				nrf_gpio_pin_set(LED_CHARGE);
				
				if( (0 == full_detect_pwr_led_blinking_cnt) && (FALSE== system_on) ){
						app_timer_stop(m_charging_pwr_led_blinking_id);
						charging_tick=0;
					
						app_timer_start(m_full_detect_pwr_led_blinking_id, FULL_DETECT_PWR_LED_BLINK_DELAY, NULL);  
				}
				

		}
		//완충 상태가 아닐 때, 
		else{
				//충전 중이면, 
				if( 0 == nrf_gpio_pin_read(CHARGE_DET)){ 
							//충전 중인데, almost full 이면, 
							if( ALMOST_FULL_BATT_ADC <= get_battery_in_volt()   ){
									//충전기를 연결한지 10분 이상이 되면 
									if( charging_time >= CHARGING_DELAY){
											nrf_gpio_pin_clear(LED_FULL);
											nrf_gpio_pin_set(LED_CHARGE);					
											
											if( (0 == full_detect_pwr_led_blinking_cnt) && (FALSE== system_on) ){
													charging_tick=0;
													app_timer_stop(m_charging_pwr_led_blinking_id);
												
													app_timer_start(m_full_detect_pwr_led_blinking_id, FULL_DETECT_PWR_LED_BLINK_DELAY, NULL);  												
											}
											
									}
									else{
											nrf_gpio_pin_set(LED_FULL);
											nrf_gpio_pin_clear(LED_CHARGE);	
											
																		
											if( FALSE == system_on){
													full_detect_pwr_led_blinking_cnt=0;
													//nrf_gpio_pin_clear(LED_PWR);
													app_timer_stop(m_full_detect_pwr_led_blinking_id);			
													if( 0 == charging_tick){
														app_timer_start(m_charging_pwr_led_blinking_id, CHARGING_PWR_LED_BLINK_DELAY, NULL);  
													}
											}
									}
							}
							else{
									nrf_gpio_pin_set(LED_FULL);
									nrf_gpio_pin_clear(LED_CHARGE);	
								
									if( FALSE == system_on){
											full_detect_pwr_led_blinking_cnt=0;
											//nrf_gpio_pin_clear(LED_PWR);
											app_timer_stop(m_full_detect_pwr_led_blinking_id);
											if( 0 == charging_tick){
													app_timer_start(m_charging_pwr_led_blinking_id, CHARGING_PWR_LED_BLINK_DELAY, NULL);  
											}
											
									}
							}
							
							charging_time++;
							if( charging_time  == 0xffffffff){
								charging_time=CHARGING_DELAY;
							}							
				}
				//충전 중이 아닌데, 
				else{
						//low battery 이면, 
						if(LOW_BATT_NOTI_ADC >= get_battery_in_volt()){							
								
								if( 0 == low_batt_led_toggle%2){
									nrf_gpio_pin_clear(LED_CHARGE);				
								}
								else{
									nrf_gpio_pin_set(LED_CHARGE);						
								}
								low_batt_led_toggle++;			
						}
						else{
								nrf_gpio_pin_set(LED_FULL);
								nrf_gpio_pin_set(LED_CHARGE);	
								low_batt_led_toggle=0;
							
								if( FALSE == system_on){
										full_detect_pwr_led_blinking_cnt=0;
										charging_tick=0;									
										nrf_gpio_pin_clear(LED_PWR);
										app_timer_stop(m_full_detect_pwr_led_blinking_id);				
										app_timer_stop(m_charging_pwr_led_blinking_id);
										
								}

							
						}
						charging_time=0;
				}
			
			
		}	

    cut_off_volt_check();    
    
}

static void reset_timeout_handler(void * p_context)
{
    NVIC_SystemReset();
}


/**@brief Function for IR, RED LED on (dimming mode) 
 */
static void power_on_timeout_handler(void * p_context)
{
    static uint8_t i;
    static uint8_t cnt=0;        
    
    if( i!=power_led_value[cnt]){
        nrf_gpio_pin_clear(LED_PWR);        
    }
    else{
        nrf_gpio_pin_set(LED_PWR);
        cnt++;
        i=0;        
    }

    i++;
    
    //finish dimming and turn on the LED 
    if( 48 == cnt){
        //Belt LED start 
        // app timer로 power led를 dim으로 킬 때, PWM으로 IR, RED LED를 키면, PWM이 순위가 더 높아서, power LED의 불빛이 흔들림. 따라서, Power LED 후, IR, RED LED를 키도록 구현. 
        nrf_gpio_pin_set(LED_PWR);
        belt_led_start();
        using_timer_start();
        
        //init values and stop timer. 
        cnt=0;
        i=0;
        app_timer_stop(m_power_on_timer_id);
     
    }
    
}

/**@brief Function for IR, RED LED on 
 */
static void ir_led_on_timeout_handler(void * p_context)
{    
    static uint8_t ir_dim_on_index=0;
    
    while (app_pwm_channel_duty_set(&PWM1, 0,pwm_value[ir_dim_on_index]) == NRF_ERROR_BUSY);    
    while (app_pwm_channel_duty_set(&PWM1, 1,pwm_value[ir_dim_on_index]) == NRF_ERROR_BUSY);
    
    ir_dim_on_index++;
    
    if( 49 == ir_dim_on_index){       
                
        while (app_pwm_channel_duty_set(&PWM1, 0,pwm_value[ir_dim_on_index]) == NRF_ERROR_BUSY);    
        while (app_pwm_channel_duty_set(&PWM1, 1,pwm_value[ir_dim_on_index]) == NRF_ERROR_BUSY);
        //app_pwm_disable(&PWM1);      
        
        app_timer_stop(m_ir_led_on_timer_id);        
        ir_dim_on_index=0;
    }  
}

/**@brief Function for change motor operation. 
 */
static void motor_mode_change_timeout_handler(void * p_context)
{    
    if( 0 == mode){
        nrf_gpio_pin_clear(MOT_1);
        nrf_gpio_pin_clear(MOT_2);
        nrf_gpio_pin_clear(MOT_3);           
    }
    else if( 1 == mode){
        nrf_gpio_pin_clear(MOT_1);
        if( 0 == motor_toggle){        
            nrf_gpio_pin_set(MOT_2);
            nrf_gpio_pin_set(MOT_3);        
            motor_toggle=1;
        }
        else{        
            nrf_gpio_pin_clear(MOT_2);
            nrf_gpio_pin_clear(MOT_3);        
            motor_toggle=0;        
        }
        
    }
    else if ( 2 == mode){
        nrf_gpio_pin_set(MOT_1);
        if( 0 == motor_toggle){        
            nrf_gpio_pin_set(MOT_2);
            nrf_gpio_pin_set(MOT_3);        
            motor_toggle=1;
        }
        else{        
            nrf_gpio_pin_clear(MOT_2);
            nrf_gpio_pin_clear(MOT_3);        
            motor_toggle=0;        
        }
        
    }

}

/**@brief Function for measuring LDI value. 
 */
static void ldi_measure_timeout_handler(void * p_context)
{    
    static int i=0;     
    static uint16_t sampling_ldi_val[4];
		static uint32_t ldi_raw_val_sum=0;
	
		int j=0;

    // interrupt ADC
		NRF_ADC->INTENSET = (ADC_INTENSET_END_Disabled << ADC_INTENSET_END_Pos);						//!< Interrupt enabled. 
	
		// config ADC
		NRF_ADC->CONFIG = (ADC_CONFIG_EXTREFSEL_None << ADC_CONFIG_EXTREFSEL_Pos) // Bits 17..16 : ADC external reference pin selection. 
		| (ADC_CONFIG_PSEL_AnalogInput3 << ADC_CONFIG_PSEL_Pos)				//	!< Use analog input 6 as analog input. 
		| (ADC_CONFIG_REFSEL_VBG << ADC_CONFIG_REFSEL_Pos)						//	!< Use internal 1.2V bandgap voltage as reference for conversion. 
		| (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) ///*!< Analog input specified by PSEL with 1/3 prescaling used as input for the conversion. 
		| (ADC_CONFIG_RES_10bit << ADC_CONFIG_RES_Pos);					//				!< 10bit ADC resolution. 
	
		
		// enable ADC		
		NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;					  													// Bit 0 : ADC enable. 	
				
		////////////////////////////////////////////////////////////////////////////
		for(j=0; j<3; j++){
				
				NRF_ADC->TASKS_START = 1;							//Start ADC sampling
			
				// wait for conversion to end
				while (!NRF_ADC->EVENTS_END)
				{}

				NRF_ADC->EVENTS_END = 0;		
					
				//Save ADC result               
				sampling_ldi_val[j] = NRF_ADC->RESULT;									
				ldi_raw_val_sum += sampling_ldi_val[j];
			  //my_printf(0, "%d\t", sampling_ldi_val[j]);
				nrf_delay_us(1);		//delay
					
				sampling_ldi_val[j]=0;	
		}		
		/////////////////////////////////////////////////////
	
		//Use the STOP task to save current. Workaround for PAN_028 rev1.1 anomaly 1.
		NRF_ADC->TASKS_STOP = 1;

		// disable ADC		
		NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Disabled;	

    i++;  
		
    if( 333 == i){			        
				//ldi_value = (ldi_raw_val_sum)/999;    
        ldi_value = ((((ldi_raw_val_sum)/999)*22)/36)+4;    

        memset(resp, 0, sizeof(resp));
        resp[0]=MEASURE_LDI_VAL;
        memcpy(resp+sizeof(uint8_t)*1, &ldi_value, sizeof(ldi_value));
        ble_custom_service_send_string_1(&m_setting_service, resp, 3);           
        
				my_printf(0, "[sum. LDI]: %d \n", ldi_raw_val_sum);
				my_printf(0, "[AVG. LDI]: %d \n", ldi_value);
		
        ldi_measure_stop();
				ldi_raw_val_sum=0;
        i=0; 
        ldi_value=0;
    }     

}

/**@brief Function for counting using time.  
 */
static void using_timeout_handler(void * p_context)
{   
    //my_printf(0, "[USIGN TIME]: %d \n", using_time);    
    using_time++;   
        
    if( LUMI_USING_TIME == using_time){      
		using_terminate();

    }
}

/**@brief Function for sending usage history to the app. (notification) 
 *
 * @details according to connection interval, it send data to the app. 
 */
static void usage_history_send_timeout_handler(void * p_context)
{
    //uint32_t err_code;
	uint32_t test_t=0;
    static int i=1;

    if( 0 == usage_history_over_flow_flag ){                
        if(m_conn_handle != BLE_CONN_HANDLE_INVALID){            
            memset(&usage_history, 0, sizeof(usage_history));  
            usage_history_send_buffer[0]=GET_USAGE_HISTORY;    
            
            if( i != p_storage_index){
                storage_load(i, usage_history, 16, 0); 
                memcpy(usage_history_send_buffer + sizeof(uint8_t)*1, usage_history, 16);
                ble_custom_service_send_string_1(&m_setting_service, usage_history_send_buffer, 17);     
				
				
				
				my_printf(0,"[Send data]:");
				for(test_t=0; test_t< 17 ; test_t++){
					my_printf(0,"%02x",usage_history_send_buffer[test_t]);
				}
				my_printf(0,"\n");
				
				
				
            }
            else{                
            }
        }           
              
        if(  i == p_storage_index){
            //i=1;
            app_timer_stop(m_usage_history_send_timer_id);
            my_printf(0,"Dddddd\n");
			ble_custom_service_send_string_1(&m_setting_service, usage_history_end, 5); 
       
        }        
        i++;
                
        if( i> p_storage_index){
            i=1;
			
        }
    }
        
    //전체 사용기록 전송 (사용 기록이 overflow 되어서 p_storage index 1부터 다시 시작했기 때문에) 
    else{
        
        if(m_conn_handle != BLE_CONN_HANDLE_INVALID){            
            memset(&usage_history, 0, sizeof(usage_history));  
            usage_history_send_buffer[0]=GET_USAGE_HISTORY;    
            
            storage_load(i, usage_history, 16, 0); 
            memcpy(usage_history_send_buffer + sizeof(uint8_t)*1, usage_history, 16);
            ble_custom_service_send_string_1(&m_setting_service, usage_history_send_buffer, 17);                   
        }       
        
        i++;        
        
        if( BLOCK_COUNT == i ){
            i=1;
            app_timer_stop(m_usage_history_send_timer_id);
			ble_custom_service_send_string_1(&m_setting_service, usage_history_end, 5); 
        }        
    }
}


/**@brief Function for buzzing motor
 */
static void ending_vib_timeout_handler(void * p_context)
{   
	ending_vib_cnt++;
		
	if( 1 == ending_vib_cnt){
		nrf_gpio_pin_clear(MOT_1);
	}
	else if( 2 == ending_vib_cnt) {
		nrf_gpio_pin_set(MOT_1);
	}
	else if( 3 == ending_vib_cnt) {
		nrf_gpio_pin_clear(MOT_1);
	}
	else if( 4 == ending_vib_cnt) {
		nrf_gpio_pin_set(MOT_1);
	}	
	else if( 5 == ending_vib_cnt) {
		//nrf_gpio_pin_clear(MOT_1);
		app_timer_stop(m_ending_vib_timer_id);
		ending_vib_cnt=0;
		//motor off 
		mode=0;      
		mode_change();
	}		
}


/**@brief Function for delay
 */
static void delay_timeout_handler(void * p_context)
{   
	end_vib_start();
}


/**@brief Function for disconnecting with app. 
 *
 * @details delay for disconnection. 
 */
static void disconnection_timeout_handler(void * p_context)
{
    disconnection_tick++;
    
    if( 1 == disconnection_tick ){        
        sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);        
    }
    else if ( 2 == disconnection_tick){
        advertising_stop();        
        app_timer_stop(m_disconnection_timer_id);
        disconnection_tick=0;
    }
}

/**@brief Function for pwer LED blinking timer for display full charge.
 */
static void full_detect_pwr_led_blinking_timeout_handler(void * p_context)
{
		
    
		full_detect_pwr_led_blinking_cnt++;
    
		if( 7 > full_detect_pwr_led_blinking_cnt ){
			if( full_detect_pwr_led_blinking_cnt%2 == 1){
				nrf_gpio_pin_set(LED_PWR);
			}
			else{
				nrf_gpio_pin_clear(LED_PWR);
			}
		}
    else{
			nrf_gpio_pin_clear(LED_PWR);
		}
		if( 16==full_detect_pwr_led_blinking_cnt){
			full_detect_pwr_led_blinking_cnt=0;
		}
		
}
//m_full_detect_pwr_led_id
/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    uint32_t err_code;    
    // Initialize timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create timers.    
    err_code = app_timer_create(&m_rtc_timer_id, APP_TIMER_MODE_REPEATED, rtc_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
    err_code = app_timer_create(&m_reset_timer_id, APP_TIMER_MODE_SINGLE_SHOT, reset_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
    err_code = app_timer_create(&m_motor_mode_timer_id, APP_TIMER_MODE_REPEATED, motor_mode_change_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
    err_code = app_timer_create(&m_power_on_timer_id, APP_TIMER_MODE_REPEATED, power_on_timeout_handler);
    APP_ERROR_CHECK(err_code); 

    err_code = app_timer_create(&m_ir_led_on_timer_id, APP_TIMER_MODE_REPEATED, ir_led_on_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
    err_code = app_timer_create(&m_ldi_measure_timer_id, APP_TIMER_MODE_REPEATED, ldi_measure_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
    err_code = app_timer_create(&m_using_timer_id, APP_TIMER_MODE_REPEATED, using_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
    err_code = app_timer_create(&m_usage_history_send_timer_id, APP_TIMER_MODE_REPEATED, usage_history_send_timeout_handler);
    APP_ERROR_CHECK(err_code); 
    
    err_code = app_timer_create(&m_disconnection_timer_id, APP_TIMER_MODE_REPEATED, disconnection_timeout_handler);
    APP_ERROR_CHECK(err_code); 
	
    err_code = app_timer_create(&m_ending_vib_timer_id, APP_TIMER_MODE_REPEATED, ending_vib_timeout_handler);
    APP_ERROR_CHECK(err_code); 	
	
		err_code = app_timer_create(&m_delay_timer_id, APP_TIMER_MODE_SINGLE_SHOT, delay_timeout_handler);
    APP_ERROR_CHECK(err_code); 	

		err_code = app_timer_create(&m_full_detect_pwr_led_blinking_id, APP_TIMER_MODE_REPEATED, full_detect_pwr_led_blinking_timeout_handler);
    APP_ERROR_CHECK(err_code); 	
		
		err_code = app_timer_create(&m_charging_pwr_led_blinking_id, APP_TIMER_MODE_REPEATED, charging_timeout_handler);
    APP_ERROR_CHECK(err_code);


}


/**@brief Function for the pwm initialization.
 */
static void pwm_init(void)
{
    ret_code_t err_code;    
    
    app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_2CH(10000L, LED_RED, LED_IR);    
		// J.K. 1KHz로 변경. 2016.11.07
    //app_pwm_config_t pwm2_cfg = APP_PWM_DEFAULT_CONFIG_1CH(1000L, PLS_OUT);   //for 1kHz clock  (100L -> 10kHz)   
		app_pwm_config_t pwm2_cfg = APP_PWM_DEFAULT_CONFIG_1CH(200L, PLS_OUT);   //for 5kHz clock  (100L -> 10kHz)   
        
    /* Set the polarity of the channel. */
    pwm1_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;
    pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;

    pwm2_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;
    
    /* Initialize and enable PWM. */
    err_code = app_pwm_init(&PWM1,&pwm1_cfg,pwm_1_ready_callback);
    err_code = app_pwm_init(&PWM2,&pwm2_cfg,pwm_2_ready_callback);
    APP_ERROR_CHECK(err_code);

}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;    

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    //get device_MAC address   
    sd_ble_gap_address_get(&mac_addr_buffer);
    
    //set device name 
    snprintf((char *)device_mac_name+(sizeof(device_mac_name)-5), 3, "%02X", mac_addr_buffer.addr[1]);   //length is n-1 
    snprintf((char *)device_mac_name+(sizeof(device_mac_name)-3), 3, "%02X", mac_addr_buffer.addr[0]);   //length is n-1 
    
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)device_mac_name,
                                          sizeof(device_mac_name)-1);  
										    
    APP_ERROR_CHECK(err_code);
                        
   // err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_UNKNOWN);
   // APP_ERROR_CHECK(err_code); 

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the RTC Service events. 
 * @details This function will be called for RTC Service events which are passed to
 *          the application.
 *
 * @param[in]   p_custom_service   p_custom_service structure.
 * @param[in]   p_evt          Event received from the RTC Service.
 *
 */
void custom_service_data_handler(ble_custom_service_t * p_custom_service, uint8_t * p_data, uint16_t length)
{
    
    uint16_t p_storage_send_index;
    uint16_t using_time_send;
    int index=0;
    
	my_printf(0, "[p_data_index] : %d \n", p_data[0]);
	
    switch(PACKET_TYPE){
        
        
        case GET_USAGE_HISTORY:                   
            //RTC, User ID(KEY) update. 
            memcpy(&current_userID, p_data+1*sizeof(uint8_t), 4);       
            memcpy(&unix_time_stamp, p_data+5*sizeof(uint8_t), 4);       
            memset(&usage_history, 0, sizeof(usage_history));       
       
            app_timer_start(m_usage_history_send_timer_id, USAGE_HISTORY_SEND_INTERVAL, NULL);

          
            break;
            
        case DEV_STATUS_REQ:
          
            //RTC, User ID(KEY) update. 
            memcpy(&current_userID, p_data+1*sizeof(uint8_t), 4);       
            memcpy(&unix_time_stamp, p_data+5*sizeof(uint8_t), 4);       
            memset(&dev_status, 0, sizeof(dev_status));       
 
            dev_status[index]=DEV_STATUS_REQ;
            index++;
        
            memcpy(dev_status+sizeof(uint8_t)*(index),&current_userID,sizeof(current_userID));        
            index=index+sizeof(current_userID);        
        
            memcpy(dev_status+sizeof(uint8_t)*(index),&unix_time_stamp,sizeof(unix_time_stamp));
            index=index+sizeof(unix_time_stamp);     
        
            dev_status[index]=using_status;
            index++;
            
            using_time_send = (uint16_t)using_time;     
            memcpy(dev_status+sizeof(uint8_t)*(index),&using_time_send,sizeof(using_time_send));
       
            index=index+sizeof(using_time_send);               
             
            dev_status[index]=mode;
            index++;
            
            dev_status[index]=battery_value;
            index++;
            
            //////////////////////////////////
            //memory status // 
            if( 0 == usage_history_over_flow_flag ){                
                p_storage_send_index= (uint16_t)p_storage_index;
            }
            else{
               p_storage_send_index= (uint16_t)BLOCK_COUNT; 
            }
            
            memcpy(dev_status+sizeof(uint8_t)*(index),&p_storage_send_index,sizeof(p_storage_send_index));        
            index=index+sizeof(p_storage_send_index);     
            
            p_storage_send_index= BLOCK_COUNT;
            
            memcpy(dev_status+sizeof(uint8_t)*(index),&p_storage_send_index,sizeof(p_storage_send_index));        
            index=index+sizeof(p_storage_send_index);     
            
            ////////////////////
   
            ble_custom_service_send_string_1(&m_setting_service, dev_status, index);      

            break;
            
            
        case MEASURE_LDI_VAL:
            
            ldi_measure_start();
            ldi_m_cnt = p_data[1];
            ldi_m_interval = p_data[2];
            

            break;
  
        case SET_ADV_INTERVAL:
            
            memset(recv_buffer, 0, sizeof(recv_buffer));	 
            memcpy(recv_buffer, p_data+1*sizeof(uint8_t), 2);       
            
            
            if( recv_buffer[1] >= 0x40){
                recv_buffer[1]=0x40;
                recv_buffer[0]=0x00;                
            }
            if( recv_buffer[1] == 0x00 && recv_buffer[0] <= 0x20){
                recv_buffer[1]=0x00;
                recv_buffer[0]=0x20;    
                
            }
            
            storage_update(SETTING_BLOCK, recv_buffer, 4, 8);              
            
            memset(send_buffer,0,sizeof(send_buffer));  
            memcpy(send_buffer+sizeof(uint8_t)*1, &recv_buffer, 2);
            send_buffer[0]=SET_ADV_INTERVAL;
            ble_custom_service_send_string_1(&m_setting_service, send_buffer, 3);                
                  
            break;
        
        case GET_ADV_INTERVAL:
            memset(send_buffer,0,sizeof(send_buffer));  
            storage_load(SETTING_BLOCK, send_buffer, 4, 8);     
            send_buffer[2]=send_buffer[1];
            send_buffer[1]=send_buffer[0];
            send_buffer[0]=GET_ADV_INTERVAL;
            ble_custom_service_send_string_1(&m_setting_service, send_buffer, 3);             
        
            break;
        
        case SET_TX_POWER: 
            memset(recv_buffer, 0, sizeof(recv_buffer));	 
            memcpy(recv_buffer, p_data+1*sizeof(uint8_t), 1);                   
            
            if( (int8_t)recv_buffer[0] <= -40){
                recv_buffer[0]=-40;                               
            }
            else if( (int8_t) recv_buffer[0] > -40 && (int8_t) recv_buffer[0] <=-30){
                recv_buffer[0]=-30;  
            }
            else if( (int8_t) recv_buffer[0] > -30 && (int8_t) recv_buffer[0] <=-20){
                recv_buffer[0]=-20;  
            }
            else if( (int8_t) recv_buffer[0] > -20 && (int8_t) recv_buffer[0] <=-16){
                recv_buffer[0]=-16;  
            }
            else if( (int8_t) recv_buffer[0] > -16 && (int8_t) recv_buffer[0] <=-12){
                recv_buffer[0]=-12;  
            }
            else if( (int8_t) recv_buffer[0] > -12 && (int8_t) recv_buffer[0] <=-8){
                recv_buffer[0]=-8;  
            }
            else if( (int8_t) recv_buffer[0] > -8 && (int8_t) recv_buffer[0] <=-4){
                recv_buffer[0]=-4;  
            }
            else if( (int8_t) recv_buffer[0] > -4 && (int8_t) recv_buffer[0] <= 0 ){
                recv_buffer[0]=0;  
            }
            else{
                recv_buffer[0]=4;  
            }
            
            storage_update(SETTING_BLOCK, recv_buffer, 4, 4);       
        
            memset(send_buffer,0,sizeof(send_buffer));  
            memcpy(send_buffer+sizeof(uint8_t)*1, &recv_buffer, 1);
            send_buffer[0]=SET_TX_POWER;
            ble_custom_service_send_string_1(&m_setting_service, send_buffer, 2);        
            break;
        
        case GET_TX_POWER:
            memset(send_buffer,0,sizeof(send_buffer));  
            storage_load(SETTING_BLOCK, send_buffer, 4, 4);                 
            send_buffer[1]=send_buffer[0];
            send_buffer[0]=GET_TX_POWER;
            ble_custom_service_send_string_1(&m_setting_service, send_buffer, 2);    
            break;

        case GET_FIRM_VER:          
            firmware_version[0]=GET_FIRM_VER;
            ble_custom_service_send_string_1(&m_setting_service, firmware_version, 4);        
            break;
                
        case ERASE_ALL_USAGE_HISTORY:
            storage_clear(BLOCK_SIZE, BLOCK_COUNT);
            usage_history_over_flow_flag=0;
               
            memset(resp, 0, sizeof(resp));
            resp[0]= ERASE_ALL_USAGE_HISTORY;
            
            resp[1] = 0x01;
            ble_custom_service_send_string_1(&m_setting_service, resp, 2);   
        
            ////////////////////초기 setting 값 다시 반영 ///// 
            default_data_save(STORAGE_SAVE_CODE);         
            //default data load
            memset(&recv_buffer, 0, sizeof(recv_buffer));
            storage_load(SETTING_BLOCK, recv_buffer, 16, 0);     
            TX_POWER   = recv_buffer[4];
            INTERVAL_1 = recv_buffer[8];
            INTERVAL_2 = recv_buffer[9];    
            memcpy(&p_storage_index, recv_buffer+sizeof(uint8_t)*12, sizeof(p_storage_index));    
        
            p_storage_index=1;        
            my_printf(0, "[P index after erase all] : %d \n", p_storage_index);            
            
            break;
			

		case MAC_ADDR_REQ:   
				 
			  						
		
			ble_custom_service_send_string_1(&m_setting_service, mac_addr_tx_buffer, 7);  
           
            break;
		
		case ADV_RESET:
			sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);   		
			ble_advertising_start(BLE_ADV_MODE_FAST);
			
			break;
        default:
            err_invalid_param[0]= p_data[0];
            ble_custom_service_send_string_1(&m_setting_service, err_invalid_param, 2);
            break;
    }
 
}


/**@brief Function for initializing Battery Service.
 */
static void bas_init(void)
{
    uint32_t       err_code;
    ble_bas_init_t bas_init_obj;

    memset(&bas_init_obj, 0, sizeof(bas_init_obj));

    bas_init_obj.evt_handler          = NULL;
    bas_init_obj.p_report_ref         = NULL;
    bas_init_obj.initial_batt_level   = 100;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init_obj.battery_level_char_attr_md.read_perm);

    err_code = ble_bas_init(&m_bas, &bas_init_obj);
    APP_ERROR_CHECK(err_code);
    
}

/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
        
    uint32_t         err_code;    
    ble_custom_service_init_t   custom_service_init;   
    ble_dfu_init_t   dfus_init;
    
    bas_init();
    
    //custom service    
    memset(&custom_service_init, 0, sizeof(custom_service_init));
    custom_service_init.data_handler = custom_service_data_handler;	
    
    err_code = ble_custom_service_init(&m_setting_service, &custom_service_init);    
    APP_ERROR_CHECK(err_code);
    
    //dfu service  
    memset(&dfus_init, 0, sizeof(dfus_init));    
    dfus_init.evt_handler   = dfu_app_on_dfu_evt;    
    dfus_init.error_handler = NULL;    
    dfus_init.evt_handler   = dfu_app_on_dfu_evt;    
    dfus_init.revision      = DFU_REVISION;    
    
    err_code = ble_dfu_init(&m_dfus, &dfus_init);    
    APP_ERROR_CHECK(err_code);    
    dfu_app_reset_prepare_set(reset_prepare);    
    dfu_app_dm_appl_instance_set(m_app_handle);    
    
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}




/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            
            break;
        case BLE_ADV_EVT_IDLE:
            //sleep_mode_enter();
            break;
        default:
            break;
    }
}


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    switch (p_ble_evt->header.evt_id)
            {
        case BLE_GAP_EVT_CONNECTED:            
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

		
			my_printf(0, "[Connected!!!]\n");
		   
            break;

        case BLE_GAP_EVT_DISCONNECTED:

            m_conn_handle = BLE_CONN_HANDLE_INVALID;
			//gap_params_init(); 
			//services_init();    
			//advertising_init();   
			//conn_params_init();
			//?????
			//ble_advertising_start(BLE_ADV_MODE_FAST);		
			my_printf(0, "[Disconnected!!!]\n");			
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    dm_ble_evt_handler(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    bsp_btn_ble_on_ble_evt(p_ble_evt);
    on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
    ble_custom_service_on_ble_evt(&m_setting_service, p_ble_evt);
    ble_dfu_on_ble_evt(&m_dfus, p_ble_evt);
    ble_bas_on_ble_evt(&m_bas, p_ble_evt); 
}

/**@brief Function for changing Lumi service mode. 
 *
 * @details 
 * MODE 0 -> LED: on / MOT 1:  off / MOT 2:  off                MOT 3:  off 
 * MODE 1 -> LED: on / MOT 1:  off / MOT 2:  1s on/off repeat   MOT 3:  1s on/off repeat 
 * MODE 2 -> LED: on / MOT 1:  on  / MOT 2:  1s on/off repeat   MOT 3:  1s on/off repeat 
 */
void mode_change(void){
    
    if( 0 == mode){
        nrf_gpio_pin_clear(MOT_1);
        nrf_gpio_pin_clear(MOT_2);
        nrf_gpio_pin_clear(MOT_3);                
    }
    else if( 1 == mode){         
        motor_toggle=0;        
        nrf_gpio_pin_clear(MOT_1);
        nrf_gpio_pin_clear(MOT_2);
        nrf_gpio_pin_clear(MOT_3);
    }
    else if( 2 == mode){
        nrf_gpio_pin_set(MOT_1);
        nrf_gpio_pin_clear(MOT_2);
        nrf_gpio_pin_clear(MOT_3);
        
    }
    else{
    }
}
/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    pstorage_sys_event_handler(sys_evt);
    ble_advertising_on_sys_evt(sys_evt);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;
    
    nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
    
    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
    
    ble_enable_params_t ble_enable_params;
    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                    PERIPHERAL_LINK_COUNT,
                                                    &ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    ble_enable_params.gatts_enable_params.service_changed = 1;
    ble_enable_params.common_enable_params.vs_uuid_count = 2;
    
    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    uint32_t err_code;
    
    switch (event)
    {
        case BSP_EVENT_DISCONNECT:
            /*
            err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            */
            break;

        case BSP_EVENT_WHITELIST_OFF:
            err_code = ble_advertising_restart_without_whitelist();
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;
            
        case BSP_EVENT_KEY_0:                
            my_printf(0, "[P_storage_index]: %d\n", p_storage_index);
            
            if( FALSE == system_on){
                // Start execution.
								full_detect_pwr_led_blinking_cnt=0;
								charging_tick=0;									
								app_timer_stop(m_full_detect_pwr_led_blinking_id);											
								app_timer_stop(m_charging_pwr_led_blinking_id);
							
                power_on_led_start();
                err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
                APP_ERROR_CHECK(err_code);          
                system_on=TRUE;                
            }
            else{             
                user_terminated =1; 
                app_timer_stop(m_power_on_timer_id);
								using_terminate();
				
            }            
            my_printf(0, "[System-on]: %d\n", system_on);
            break;
            
        //down button
        case BSP_EVENT_KEY_1:                  
            if( FALSE == system_on){
                
            }
            else{
                if( 0 < mode){
                   mode--;
                }
                else{
                    
                }
                mode_change();
            }         
            break;
            
        //up button
        case BSP_EVENT_KEY_2:           
            if( FALSE == system_on){
                
            }
            else{
                if( 2 > mode){
                    mode++;
                }
                else{
                    
                }
                mode_change();
            }            
            break;

        
        default:
            break;
    }
}


/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           ret_code_t        event_result)
{
    APP_ERROR_CHECK(event_result);

#ifdef BLE_DFU_APP_SUPPORT
    if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }
#endif // BLE_DFU_APP_SUPPORT

    return NRF_SUCCESS;
}


/**@brief Function for the Device Manager initialization.
 */
static void device_manager_init()
{
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = NULL};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.lesc         = SEC_PARAM_LESC;
    register_param.sec_param.keypress     = SEC_PARAM_KEYPRESS;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for converting advertising interval. 
 */
uint16_t get_adv_interval(){
        
    uint16_t interval;
    
    setting_data_update();
    
    //convert 
    interval = INTERVAL_1;
    interval|= INTERVAL_2 << 1*8;
    
    //interval check 
    if(interval <= 0x0020)
        interval = 0x0020;
    else if (interval >= 0x4000)
        interval = 0x4000;
    
    //0x0640 is 1000ms interval
    return interval;
}


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t scanrsp;
    
     ble_uuid_t adv_uuids[] = {{BLE_CUSTOM_SERVICE_UUID, m_setting_service.uuid_type}};
     
    // Build advertising data struct to pass into @ref ble_advertising_init.
    memset(&advdata, 0, sizeof(advdata));    
     
    advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    advdata.uuids_complete.p_uuids  = adv_uuids;
     
    //advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance      = false;
    advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
       
    memset(&scanrsp, 0, sizeof(scanrsp));
    scanrsp.name_type = BLE_ADVDATA_FULL_NAME;     
     
    ble_adv_modes_config_t options = {0};
    options.ble_adv_fast_enabled  = BLE_ADV_FAST_ENABLED;
    options.ble_adv_fast_interval = get_adv_interval();
    options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;
  
    err_code = ble_advertising_init(&advdata, &scanrsp, &options, on_adv_evt, NULL);
    APP_ERROR_CHECK(err_code);
    
    sd_ble_gap_tx_power_set(TX_POWER);
}


/**@brief Function for initializing buttons and leds.
 */
static void buttons_leds_init()
{
    //bsp_event_t startup_event;

    uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
                                 APP_TIMER_TICKS(100, APP_TIMER_PRESCALER), 
                                 bsp_event_handler);

    APP_ERROR_CHECK(err_code);

		//J.K. 2016.11.07 버튼 0에 대한 push event를 long push 로 변경함. 
    err_code = bsp_event_to_button_action_assign(0, BSP_BUTTON_ACTION_LONG_PUSH, BSP_EVENT_KEY_0);
    APP_ERROR_CHECK(err_code);
    
    err_code = bsp_event_to_button_action_assign(1, BSP_BUTTON_ACTION_PUSH, BSP_EVENT_KEY_1);
    APP_ERROR_CHECK(err_code);
    
    err_code = bsp_event_to_button_action_assign(2, BSP_BUTTON_ACTION_PUSH, BSP_EVENT_KEY_2);
    APP_ERROR_CHECK(err_code);

}

/**@brief Function for initializing pins. 
 */
static void pin_init(void)
{    
    nrf_gpio_cfg_output(LED_PWR);    
    nrf_gpio_cfg_output(LED_RED);    
    nrf_gpio_cfg_output(LED_IR);    
    nrf_gpio_cfg_output(MOT_1);
    nrf_gpio_cfg_output(MOT_2);
    nrf_gpio_cfg_output(MOT_3);      
    nrf_gpio_cfg_output(PLS_OUT);    
    nrf_gpio_cfg_output(LED_CHARGE);    
    nrf_gpio_cfg_output(LED_FULL);    
    
    nrf_gpio_pin_set(LED_CHARGE);
    nrf_gpio_pin_set(LED_FULL);
    nrf_gpio_pin_clear(LED_PWR);
   
    nrf_gpio_cfg_input(CHARGE_DET, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_cfg_input(FULL_DET, NRF_GPIO_PIN_NOPULL);
    
}

void cut_off_volt_check(void){
    
    //cut off 
    if( CUT_OFF_BATT_ADC >= get_battery_in_volt()){
     
            
        app_timer_stop(m_using_timer_id);    
        mode=0;
        mode_change();
        //LED off 
        while (app_pwm_channel_duty_set(&PWM1, 0,pwm_value[0]) == NRF_ERROR_BUSY);    
        while (app_pwm_channel_duty_set(&PWM1, 1,pwm_value[0]) == NRF_ERROR_BUSY);
        
        app_pwm_disable(&PWM1);     
         
        nrf_gpio_pin_clear(LED_IR);
        nrf_gpio_pin_clear(LED_RED);
        nrf_gpio_pin_clear(LED_PWR);
        
        system_on=FALSE;
        
        sd_power_system_off();
    }

    else if(LOW_BATT_ADC > get_battery_in_volt()){
				
				using_terminate();

    }
}

/**@brief Function for saving default data 
 */
void default_data_save(int op_code){
    
    switch(op_code){        
        case STORAGE_UPDATE_CODE:
            storage_update(SETTING_BLOCK, default_setting_data, 16, 0);		
            break;
        
        case STORAGE_SAVE_CODE:
            storage_save(SETTING_BLOCK, default_setting_data, 16, 0);		
            break;
    }
}

/**@brief Function for checking first boot 
 */
bool first_boot_check(void)
{    
	uint8_t first_boot_cmp[4];
	
	storage_load(SETTING_BLOCK, first_boot_cmp, 4, 0);
	
	if( (0xff == first_boot_cmp[0])){
        first_boot_flag=0x01;      
    }
	else{
        first_boot_flag=0x00;     
    }
	return (bool) first_boot_flag;
}

/**@brief Function for setting BLE setting values. 
 */
void setting_data_update(void){
    //default dat load
    memset(&recv_buffer, 0, sizeof(recv_buffer));
    storage_load(SETTING_BLOCK, recv_buffer, 16, 0);           
    
    TX_POWER   = recv_buffer[4];
    INTERVAL_1 = recv_buffer[8];
    INTERVAL_2 = recv_buffer[9];
}

/**@brief Function for the load data from pstorage block.
 */
static void initial_data_load(void){    
   
    //first boot check.         
    if( true == first_boot_check()){
        //fisrt boot load default value data        
        default_data_save(STORAGE_SAVE_CODE);        
        //for saving to p_storage
        app_timer_start(m_reset_timer_id, RESET_TIMER_INTERVAL, NULL);
        
    }    
    
    //default dat load
    memset(&recv_buffer, 0, sizeof(recv_buffer));
    storage_load(SETTING_BLOCK, recv_buffer, 16, 0);           
    
    TX_POWER   = recv_buffer[4];
    INTERVAL_1 = recv_buffer[8];
    INTERVAL_2 = recv_buffer[9];    
    memcpy(&p_storage_index, recv_buffer+sizeof(uint8_t)*12, sizeof(p_storage_index));

}

/**@brief Function for the Power manager.
 */
void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for swapping mac address start<->end
 */
void swap_mac_addr(void)
{
	uint8_t swap_buffer;	
	
	swap_buffer=mac_addr_buffer.addr[0];	
	mac_addr_buffer.addr[0]=mac_addr_buffer.addr[5];
	mac_addr_buffer.addr[5]=swap_buffer;	
		
	swap_buffer=mac_addr_buffer.addr[1];	
	mac_addr_buffer.addr[1]=mac_addr_buffer.addr[4];
	mac_addr_buffer.addr[4]=swap_buffer;	
		
	swap_buffer=mac_addr_buffer.addr[2];	
	mac_addr_buffer.addr[2]=mac_addr_buffer.addr[3];
	mac_addr_buffer.addr[3]=swap_buffer;
	
	memcpy(mac_addr_tx_buffer+sizeof(uint8_t)*1, mac_addr_buffer.addr, sizeof(uint8_t)*6);
	
}



/**@brief Function for application main entry.
 */
int main(void)
{
    uint32_t err_code;
    my_printf(0, "[-Application start-]\n");
    
    // Initialize.
    pin_init(); 
    timers_init();   
    buttons_leds_init(); 
    pwm_init();
    ble_stack_init();   
    device_manager_init();    
    my_p_storage_init(BLOCK_SIZE,BLOCK_COUNT);          
    initial_data_load();        
    gap_params_init(); 
    services_init();    
    advertising_init();   
    conn_params_init();

    //RTC timer start 
    err_code = app_timer_start(m_rtc_timer_id, RTC_TIMER_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
   
    //mode change timer start 
    app_timer_start(m_motor_mode_timer_id, MOTOR_MODE_CHANGE_TIMER_INTERVAL, NULL);              
    
		swap_mac_addr();	
		
								
    // Enter main loop.
    for (;;)
    {
        power_manage();
    }
}

/**
 * @}
 * ROM, RAM 사용
* ROM: 전체 256kB(0x040000) = "SD: 0x1B000" + "app size: 0x???" + "Free (for DFU): 0x???" : + "App data: 0x????" + "DFU bootloader : 0x04000" 
* RAM: 전체  16KB(0x4000)  = "SD: 0x2000(최소 0x13c8인데, ble stack (multi link), attrubute, security, service 개수에따라 다름" + "app사용:0x1A00 "  + "call stack 0x600" 
* 따라서, ROM은 1B000 부터 size는 0x20000, RAM은0x20002000부터 size는 0x1A00으로 설정 함. 
* 추가. BLE stack에 따른 RAM 사용은 components\softdevice\common\soft_device_handler\app_ram_base.h 를 참고하면 됨. 
// pstorage block은 16byte씩 640개 만들기. 전부 10KB로 잡기. 
//2016.11.07 수정사항.  (fw_ver 0.6.0)  
//1. 전원키 2초 long button 으로 수정.
//2. LDI output을 1kHz로 변경
//3. 모터 주기를 1초 간격으로 변경. 
//4. 사용 시간을 30분으로 변경. 
 */
