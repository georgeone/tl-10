
#include "common.h"
#include "ble_custom_service.h"

#define BLE_CUSTOM_SERVICE_UUID 0x0000


/**@brief     Function for handling the @ref BLE_GAP_EVT_CONNECTED event from the S110 SoftDevice.
 *
 * @param[in] p_custom_service     Custom Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_connect(ble_custom_service_t * p_custom_service, ble_evt_t * p_ble_evt)
{
    p_custom_service->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/**@brief     Function for handling the @ref BLE_GAP_EVT_DISCONNECTED event from the S110
 *            SoftDevice.
 *
 * @param[in] p_custom_service     Custom Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_disconnect(ble_custom_service_t * p_custom_service, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_custom_service->conn_handle = BLE_CONN_HANDLE_INVALID;
}



/**@brief     Function for handling the @ref BLE_GATTS_EVT_WRITE event from the S110 SoftDevice.
 *
 * @param[in] p_custom_service     Custom Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_write(ble_custom_service_t * p_custom_service, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    
    if (
        (p_evt_write->handle == p_custom_service->rx_1_handles.cccd_handle)
        &&
        (p_evt_write->len == 2)
       )
    {
        if (ble_srv_is_notification_enabled(p_evt_write->data))
        {
            p_custom_service->is_notification_enabled = true;
        }
        else
        {
            p_custom_service->is_notification_enabled = false;
        }
    }
    
    else if (
             (p_evt_write->handle == p_custom_service->tx_handles.value_handle)
             &&
             (p_custom_service->data_handler != NULL)
            )
    {
        p_custom_service->data_handler(p_custom_service, p_evt_write->data, p_evt_write->len);
    }
    else
    {
        // Do Nothing. This event is not relevant to this service.
    }
}



void ble_custom_service_on_ble_evt(ble_custom_service_t * p_custom_service, ble_evt_t * p_ble_evt)
{
    if ((p_custom_service == NULL) || (p_ble_evt == NULL))
    {
        return;
    }

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_custom_service, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_custom_service, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_custom_service, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}



/**@brief       Function for adding RX characteristic.
 *
 * @param[in]   p_setting       Setting Service structure.
 * @param[in]   p_setting_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rx_1_char_add(ble_custom_service_t * p_custom_service, const ble_custom_service_init_t * p_custom_service_init)
{
    /**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;	  
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.notify = 1;       
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type             = p_custom_service->uuid_type;
    ble_uuid.uuid             = BLE_NOTIFY_1_CHARACTERISTIC;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc              = BLE_GATTS_VLOC_STACK;		
    attr_md.rd_auth           = 0;
    attr_md.wr_auth           = 0;
    attr_md.vlen              = 1;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;		
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 60;//sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = 60;//BLE_OID_MAX_RX_CHAR_LEN;//20
	attr_char_value.p_value      = NULL;             //추가 
    
    return sd_ble_gatts_characteristic_add(p_custom_service->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_custom_service->rx_1_handles);
    /**@snippet [Adding proprietary characteristic to S110 SoftDevice] */

}

/**@brief       Function for adding TX characteristic.
 *
 * @param[in]   p_custom_service         Custom Service structure.
 * @param[in]   p_custom_service_init      Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t tx_char_add(ble_custom_service_t * p_custom_service, const ble_custom_service_init_t * p_custom_service_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.write            = 1;
    char_md.char_props.notify           = 1;      
    //char_md.char_props.read             = 1;
    char_md.p_char_user_desc            = NULL;
    char_md.p_char_pf                   = NULL;
    char_md.p_user_desc_md              = NULL;
    char_md.p_cccd_md                   = NULL;
    char_md.p_sccd_md                   = NULL;
    
    ble_uuid.type                       = p_custom_service->uuid_type;
    ble_uuid.uuid                       = BLE_WRITE_CHARACTERISTIC;
    
    memset(&attr_md, 0, sizeof(attr_md));

   // BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc                        = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth                     = 0;
    attr_md.wr_auth                     = 0;
    attr_md.vlen                        = 1;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid              = &ble_uuid;
    attr_char_value.p_attr_md           = &attr_md;
    attr_char_value.init_len            = 1;
    attr_char_value.init_offs           = 0;
    attr_char_value.max_len             = BLE_CUSTOM_MAX_TX_CHAR_LEN;
  
    return sd_ble_gatts_characteristic_add(p_custom_service->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_custom_service->tx_handles);
}



uint32_t ble_custom_service_init(ble_custom_service_t * p_custom_service, const ble_custom_service_init_t * p_custom_service_init)
{
    uint32_t        err_code;
    ble_uuid_t      ble_uuid;
    ble_uuid128_t   custom_service_base_uuid = {0x4C, 0x75, 0x6D, 0x69, 0x2D, 0x42, 0x23, 0x81,
                                                0x31, 0x30, 0x08, 0x21, 0x00, 0x00, 0x00, 0x00};
													 
    if ((p_custom_service == NULL) || (p_custom_service_init == NULL))
    {
        return NRF_ERROR_NULL;
    }
    
    // Initialize service structure.
    p_custom_service->conn_handle              = BLE_CONN_HANDLE_INVALID;
    p_custom_service->data_handler             = p_custom_service_init->data_handler;
    p_custom_service->is_notification_enabled  = true;
    

    /**@snippet [Adding proprietary Service to S110 SoftDevice] */

    // Add custom base UUID.
    err_code = sd_ble_uuid_vs_add(&custom_service_base_uuid, &p_custom_service->uuid_type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    ble_uuid.type = p_custom_service->uuid_type;
    ble_uuid.uuid = BLE_CUSTOM_SERVICE_UUID;

    // Add service.
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_custom_service->service_handle);
    /**@snippet [Adding proprietary Service to S110 SoftDevice] */
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    

    // Add TX Characteristic.
    err_code = tx_char_add(p_custom_service, p_custom_service_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }	
    
    /*
    // Add RX Characteristic.
    err_code = rx_1_char_add(p_custom_service, p_custom_service_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    */

    return NRF_SUCCESS;
}





uint32_t ble_custom_service_send_string_1(ble_custom_service_t * p_custom_service, uint8_t * string, uint16_t length)
{
    ble_gatts_hvx_params_t hvx_params;

    if (p_custom_service == NULL)
    {
        return NRF_ERROR_NULL;
    }
    
    if ((p_custom_service->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_custom_service->is_notification_enabled))
    {
        return NRF_ERROR_INVALID_STATE;
    }
    
    if (length > BLE_CUSTOM_MAX_DATA_LEN)
    {
        return NRF_ERROR_INVALID_PARAM;
    }
    
    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_custom_service->tx_handles.value_handle;
    hvx_params.p_data = string;
    hvx_params.p_len  = &length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    
    return sd_ble_gatts_hvx(p_custom_service->conn_handle, &hvx_params);
		
}

uint32_t ble_custom_service_send_adc_1(ble_custom_service_t * p_custom_service, int16_t * adc_value, uint16_t length)
{
    ble_gatts_hvx_params_t hvx_params;

    if (p_custom_service == NULL)
    {
        return NRF_ERROR_NULL;
    }
    
    if ((p_custom_service->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_custom_service->is_notification_enabled))
    {
        return NRF_ERROR_INVALID_STATE;
    }
    
    if (length > BLE_CUSTOM_MAX_DATA_LEN)
    {
        return NRF_ERROR_INVALID_PARAM;
    }
    
    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_custom_service->tx_handles.value_handle;
    hvx_params.p_data = (uint8_t*) adc_value;
    hvx_params.p_len  = &length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    
    return sd_ble_gatts_hvx(p_custom_service->conn_handle, &hvx_params);
		
}

uint32_t ble_custom_service_send_string_2(ble_custom_service_t * p_custom_service, uint8_t * string, uint16_t length)
{
    ble_gatts_hvx_params_t hvx_params;

    if (p_custom_service == NULL)
    {
        return NRF_ERROR_NULL;
    }
    
    if ((p_custom_service->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_custom_service->is_notification_enabled))
    {
        return NRF_ERROR_INVALID_STATE;
    }
    
    if (length > BLE_CUSTOM_MAX_DATA_LEN)
    {
        return NRF_ERROR_INVALID_PARAM;
    }
    
    memset(&hvx_params, 0, sizeof(hvx_params));

    //hvx_params.handle = p_custom_service->rx_2_handles.value_handle;
    hvx_params.handle = p_custom_service->tx_handles.value_handle;
    hvx_params.p_data = string;
    hvx_params.p_len  = &length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    
    return sd_ble_gatts_hvx(p_custom_service->conn_handle, &hvx_params);

}







